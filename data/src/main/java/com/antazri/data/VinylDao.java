package com.antazri.data;

import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;

import java.util.Collection;
import java.util.List;

public interface VinylDao extends AbstractBasicDao<Vinyl, Integer> {

    List<Vinyl> findByTitle(String title);

    List<Vinyl> findByArtist(Artist artist);

    List<Vinyl> findByPublicationYear(int year);

    List<Vinyl> findByDateRange(int start, int end);

    List<Vinyl> findByCategory(Category category);
}
