package com.antazri.data;

import com.antazri.model.Artist;

import java.util.List;

public interface ArtistDao extends AbstractBasicDao<Artist, Integer> {

    List<Artist> findByName(String name);

}
