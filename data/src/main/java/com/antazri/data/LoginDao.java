package com.antazri.data;

import com.antazri.model.user.Login;

import java.util.Optional;

public interface LoginDao {

    Optional<Login> findByEmail(String email);
}
