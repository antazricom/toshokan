package com.antazri.data;

import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface BookDao extends AbstractBasicDao<Book, Integer> {

    List<Book> findByTitle(String title);

    List<Book> findByAuthor(Author author);

    List<Book> findByCategory(Category category);

    Optional<Book> findByIsbn(String isbn);

    List<Book> findByPublicationYear(int year);

    List<Book> findByDateRange(int start, int end);
}
