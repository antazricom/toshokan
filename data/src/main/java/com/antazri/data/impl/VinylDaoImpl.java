package com.antazri.data.impl;

import com.antazri.data.VinylDao;
import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("vinylDao")
@Transactional
public class VinylDaoImpl implements VinylDao {

    private static final Logger logger = LogManager.getLogger(VinylDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Vinyl> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Vinyl.class, id));
    }

    @Override
    public List<Vinyl> findByTitle(String title) {
        Query query = entityManager.createNamedQuery("Vinyl.FindByTitle", Vinyl.class);
        query.setParameter("title", "%" + title + "%");
        return query.getResultList();
    }

    @Override
    public List<Vinyl> findByArtist(Artist artist) {
        Query query = entityManager.createNamedQuery("Vinyl.FindByArtist", Vinyl.class);
        query.setParameter("id", artist.getId());
        return query.getResultList();
    }

    @Override
    public List<Vinyl> findByPublicationYear(int year) {
        Query query = entityManager.createNamedQuery("Vinyl.FindByPublicationYear", Vinyl.class);
        query.setParameter("year", year);
        return query.getResultList();
    }

    @Override
    public List<Vinyl> findByDateRange(int start, int end) {
        Query query = entityManager.createNamedQuery("Vinyl.FindByDateRange", Vinyl.class);
        query.setParameter("start", start);
        query.setParameter("end", end);
        return query.getResultList();
    }

    @Override
    public List<Vinyl> findByCategory(Category category) {
        Query query = entityManager.createNamedQuery("Vinyl.FindByCategory", Vinyl.class);
        query.setParameter("id", category.getId());
        return query.getResultList();
    }

    @Override
    public List<Vinyl> findAll() {
        Query query = entityManager.createNamedQuery("Vinyl.FindAll", Vinyl.class);
        return query.getResultList();
    }

    @Override
    public Vinyl add(Vinyl vinyl) {
        entityManager.persist(vinyl);
        return vinyl;
    }

    @Override
    public List<Vinyl> add(Collection<Vinyl> vinyls) {
        List<Vinyl> addedVinyls = new ArrayList<>();

        for (Vinyl vinyl : vinyls) {
            entityManager.persist(vinyl);
            addedVinyls.add(vinyl);
        }

        return addedVinyls;
    }

    @Override
    public Vinyl update(Vinyl vinyl) {
        return entityManager.merge(vinyl);
    }

    @Override
    public List<Vinyl> update(Collection<Vinyl> vinyls) {
        List<Vinyl> updatedVinyls = new ArrayList<>();

        for (Vinyl vinyl : vinyls) {
            updatedVinyls.add(entityManager.merge(vinyl));
        }

        return updatedVinyls;
    }

    @Override
    public void delete(Vinyl vinyl) {
        entityManager.remove(
                entityManager.contains(vinyl)
                        ? vinyl
                        : entityManager.merge(vinyl)
        );
    }

    @Override
    public void delete(Collection<Vinyl> vinyls) {
        for (Vinyl vinyl : vinyls) {
            entityManager.remove(
                    entityManager.contains(vinyl)
                            ? vinyl
                            : entityManager.merge(vinyl)
            );
        }
    }
}
