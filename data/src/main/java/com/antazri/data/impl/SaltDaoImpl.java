package com.antazri.data.impl;

import com.antazri.data.SaltDao;
import com.antazri.model.security.Salt;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Optional;

@Repository
public class SaltDaoImpl implements SaltDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Salt> findByLoginId(int id) {
        Query query = entityManager.createNamedQuery("Salt.FindByLogin", Salt.class);
        query.setParameter("id", id);

        try {
            return Optional.ofNullable((Salt) query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
}
