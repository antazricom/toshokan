package com.antazri.data.impl;

import com.antazri.data.CategoryDao;
import com.antazri.model.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("categoryDao")
@Transactional
public class CategoryDaoImpl implements CategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Category> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Category.class, id));
    }

    @Override
    public List<Category> findByName(String name) {
        Query query = entityManager.createNamedQuery("Category.FindByName", Category.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }

    @Override
    public List<Category> add(Collection<Category> categories) {
        List<Category> addedCategories = new ArrayList<>();

        for (Category category : categories) {
            entityManager.persist(category);
            addedCategories.add(category);
        }

        return addedCategories;
    }

    @Override
    public List<Category> update(Collection<Category> categories) {
        List<Category> updatedCategories = new ArrayList<>();

        for (Category category : categories) {
            updatedCategories.add(entityManager.merge(category));
        }

        return updatedCategories;
    }

    @Override
    public void delete(Collection<Category> categories) {
        for (Category category : categories) {
            entityManager.remove(
                    entityManager.contains(category)
                            ? category
                            : entityManager.merge(category)
            );
        }
    }

    @Override
    public List<Category> findAll() {
        Query query = entityManager.createNamedQuery("Category.FindAll", Category.class);
        return query.getResultList();
    }

    @Override
    public Category add(Category category) {
        entityManager.persist(category);
        return category;
    }

    @Override
    public Category update(Category category) {
        return entityManager.merge(category);
    }

    @Override
    public void delete(Category category) {
        entityManager.remove(
                entityManager.contains(category)
                        ? category
                        : entityManager.merge(category)
        );
    }
}
