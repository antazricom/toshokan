package com.antazri.data.impl;

import com.antazri.data.LoginDao;
import com.antazri.model.user.Login;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Optional;

@Repository
public class LoginDaoImpl implements LoginDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Login> findByEmail(String email) {
        Query query = entityManager.createNamedQuery("Login.FindByEmail", Login.class);
        query.setParameter("email", email);

        try {
            return Optional.ofNullable((Login) query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
}
