package com.antazri.data.impl;

import com.antazri.data.BookDao;
import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Component @Repository de Book implémentant l'interface IBookDao, il permet l'accès et l'envoie des objets Book
 * dans la base de données via le @PersistenceContext de Hibernate
 */
@Repository
@Qualifier("bookDao")
@Transactional
public class BookDaoImpl implements BookDao {

    private final Logger logger = LogManager.getLogger(BookDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Book> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Book.class, id));
    }

    @Override
    public List<Book> findByTitle(String title) {
        Query query = entityManager.createNamedQuery("Book.FindByTitle", Book.class);
        query.setParameter("title", "%" + title + "%");
        return query.getResultList();
    }

    @Override
    public List<Book> findByAuthor(Author author) {
        Query query = entityManager.createNamedQuery("Book.FindByAuthor", Book.class);
        query.setParameter("id", author.getId());
        return query.getResultList();
    }

    @Override
    public List<Book> findByCategory(Category category) {
        Query query = entityManager.createNamedQuery("Book.FindByCategory", Book.class);
        query.setParameter("id", category.getId());
        return query.getResultList();
    }

    @Override
    public Optional<Book> findByIsbn(String isbn) {
        Query query = entityManager.createNamedQuery("Book.FindByIsbn", Book.class);
        query.setParameter("isbn", isbn);
        return Optional.ofNullable((Book) query.getSingleResult());
    }

    @Override
    public List<Book> findByPublicationYear(int year) {
        Query query = entityManager.createNamedQuery("Book.FindByPublicationYear", Book.class);
        query.setParameter("year", year);
        return query.getResultList();
    }

    @Override
    public List<Book> findByDateRange(int start, int end) {
        Query query = entityManager.createNamedQuery("Book.FindByDateRange", Book.class);
        query.setParameter("start", start);
        query.setParameter("end", end);
        return query.getResultList();
    }

    @Override
    public List<Book> add(Collection<Book> books) {
        List<Book> addedBooks = new ArrayList<>();

        for (Book book : books) {
            entityManager.persist(book);
            addedBooks.add(book);
        }

        return addedBooks;
    }

    @Override
    public List<Book> update(Collection<Book> books) {
        List<Book> updatedBooks = new ArrayList<>();

        for (Book book : books) {
            updatedBooks.add(entityManager.merge(book));
        }

        return updatedBooks;
    }

    @Override
    public void delete(Collection<Book> books) {
        for (Book book : books) {
            entityManager.remove(
                    entityManager.contains(book)
                            ? book
                            : entityManager.merge(book)
            );
        }
    }

    @Override
    public List<Book> findAll() {
        Query query = entityManager.createNamedQuery("Book.FindAll", Book.class);
        return query.getResultList();
    }

    @Override
    public Book add(Book book) {
        entityManager.persist(book);
        return book;
    }

    @Override
    public Book update(Book book) {
        return entityManager.merge(book);
    }

    @Override
    public void delete(Book book) {
        entityManager.remove(
                entityManager.contains(book)
                        ? book
                        : entityManager.merge(book)
        );
    }
}
