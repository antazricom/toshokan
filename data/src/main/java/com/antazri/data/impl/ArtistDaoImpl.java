package com.antazri.data.impl;

import com.antazri.data.ArtistDao;
import com.antazri.model.Artist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("artistDao")
@Transactional
public class ArtistDaoImpl implements ArtistDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Artist> findById(Integer id) {
        return Optional.of(entityManager.find(Artist.class, id));
    }

    @Override
    public List<Artist> findByName(String name) {
        Query query = entityManager.createNamedQuery("Artist.FindByName", Artist.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }

    @Override
    public List<Artist> findAll() {
        Query query = entityManager.createNamedQuery("Artist.FindAll", Artist.class);
        return query.getResultList();
    }

    @Override
    public Artist add(Artist artist) {
        entityManager.persist(artist);
        return artist;
    }

    @Override
    public List<Artist> add(Collection<Artist> artists) {
        List<Artist> addedArtists = new ArrayList<>();

        for (Artist artist : artists) {
            entityManager.persist(artist);
            addedArtists.add(artist);
        }

        return addedArtists;
    }

    @Override
    public Artist update(Artist artist) {
        entityManager.merge(artist);
        return artist;
    }

    @Override
    public List<Artist> update(Collection<Artist> artists) {
        List<Artist> updatedArtists = new ArrayList<>();

        for (Artist artist : artists) {
            updatedArtists.add(entityManager.merge(artist));
        }

        return updatedArtists;
    }

    @Override
    public void delete(Artist artist) {
        entityManager.remove(entityManager.contains(artist)
                ? artist
                : entityManager.merge(artist)
        );
    }

    @Override
    public void delete(Collection<Artist> artists) {
        for (Artist artist : artists) {
            entityManager.remove(entityManager.contains(artist)
                    ? artist
                    : entityManager.merge(artist)
            );
        }
    }
}
