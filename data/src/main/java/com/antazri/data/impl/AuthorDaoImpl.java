package com.antazri.data.impl;

import com.antazri.data.AuthorDao;
import com.antazri.model.Author;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Component @Repository de Author implémentant l'interface IAuthorDao, il permet l'accès et l'envoie des objets Author
 * dans la base de données via le @PersistenceContext de Hibernate
 */
@Repository
@Qualifier("authorDao")
@Transactional
public class AuthorDaoImpl implements AuthorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Author> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Author.class, id));
    }

    @Override
    public List<Author> findByName(String name) {
        Query query = entityManager.createNamedQuery("Author.FindByName", Author.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }

    @Override
    public List<Author> add(Collection<Author> authors) {
        List<Author> persistedAuthors = new ArrayList<>();

        for (Author author : authors) {
            entityManager.persist(author);
            persistedAuthors.add(author);
        }

        return persistedAuthors;
    }

    @Override
    public List<Author> update(Collection<Author> authors) {
        List<Author> updatedAuthors = new ArrayList<>();

        for (Author author : authors) {
            updatedAuthors.add(entityManager.merge(author));
        }

        return updatedAuthors;
    }

    @Override
    public void delete(Collection<Author> authors) {
        for (Author author : authors) {
            entityManager.remove(entityManager.contains(author)
                    ? author
                    : entityManager.merge(author)
            );
        }
    }

    @Override
    public List<Author> findAll() {
        Query query = entityManager.createNamedQuery("Author.FindAll", Author.class);
        return query.getResultList();
    }

    @Override
    public Author add(Author author) {
        entityManager.persist(author);
        return author;
    }

    @Override
    public Author update(Author author) {
        return entityManager.merge(author);
    }

    @Override
    public void delete(Author author) {
        entityManager.remove(entityManager.contains(author)
                ? author
                : entityManager.merge(author)
        );
    }
}
