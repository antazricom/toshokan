package com.antazri.data;

import com.antazri.model.security.Salt;

import java.util.Optional;

public interface SaltDao {

    Optional<Salt> findByLoginId(int id);
}
