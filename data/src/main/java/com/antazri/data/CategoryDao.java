package com.antazri.data;

import com.antazri.model.Category;

import java.util.List;

public interface CategoryDao extends AbstractBasicDao<Category, Integer> {

    List<Category> findByName(String name);
}
