package com.antazri.data;

import com.antazri.model.Author;

import java.util.List;

public interface AuthorDao extends AbstractBasicDao<Author, Integer> {

    List<Author> findByName(String name);

}
