package com.antazri.data;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AbstractBasicDao<T, ID> {

    Optional<T> findById(ID id);

    List<T> findAll();

    T add(T t);

    List<T> add(Collection<T> t);

    T update(T t);

    List<T> update(Collection<T> t);

    void delete(T t);

    void delete(Collection<T> t);
}
