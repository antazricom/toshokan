package com.antazri.data.impl;

import com.antazri.data.CategoryDao;
import com.antazri.model.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
class CategoryDaoImplIntegrationTest {

    @Autowired
    private CategoryDao categoryDao;

    @Test
    @DisplayName("FindAll should return all instances")
    void whenFindAll_shouldReturnAllCategoriesFromDb() {
        // When
        final List<Category> categories = new ArrayList<>(categoryDao.findAll());

        // Then
        assertEquals(9, categories.size());
    }

    @Test
    @DisplayName("FindByName should return all instances containing request parameter")
    void whenFindAll_shouldReturnCategoriesContainingParameter() {
        // Given
        String name = "litt";

        // When
        final List<Category> categories = categoryDao.findByName(name);

        // Then
        assertEquals(4, categories.size());
    }

    @Test
    @DisplayName("FindById should return instance with same id")
    void whenFindById_shouldReturnSingleWithSameId() {
        // When
        final Optional<Category> categoryOpt = categoryDao.findById(1);

        // Then
        assertAll(() -> {
            assertTrue(categoryOpt.isPresent());
            assertEquals("book", categoryOpt.get().getType());
        });
    }

    @Test
    @DisplayName("Add should return category with id")
    void whenAddingNewCategory_shouldReturnWithId() {
        // Given
        Category category = new Category("Biography", "book");

        // When
        final Category c = categoryDao.add(category);

        // Then
        assertAll(() -> {
            assertNotNull(c.getId());
            assertEquals(1, categoryDao.findByName("Biography").size());
        });
    }

    @Test
    @DisplayName("Update should return category with updates")
    void whenUpdatingCategory_shouldUpdatedCategory() {
        // Given
        Category category = new Category("German Literature", "book");
        category.setId(1);

        // When
        final Category c = categoryDao.update(category);

        // Then
        assertEquals("German Literature", categoryDao.findById(1).get().getName());
    }

    @Test
    @DisplayName("Delete should remove object quietly")
    void whenremovingCategory_shouldDeleteInstanceQuietly() {
        // When
        Category category = new Category("test", "book");
        category.setId(3);

        // Then
        assertDoesNotThrow(() -> categoryDao.delete(category));
    }
}