package com.antazri.data.impl;

import com.antazri.data.VinylDao;
import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
class VinylDaoIntegrationTest {

    @Autowired
    private VinylDao vinylDao;

    @Test
    void whenFindingById_shouldReturnInstanceAsOptional() {
        // Given
        int id = 1;

        // When
        Optional<Vinyl> vinyl = vinylDao.findById(id);

        // Then
        assertAll(() -> {
            assertTrue(vinyl.isPresent());
            assertEquals("The Joshua Tree", vinyl.get().getTitle());
        });
    }

    @Test
    void whenFindingByAuthor_shouldReturnInstancesRelatedToSameAuthor() {
        // Given
        Artist a = new Artist(1, "Test");

        // When
        List<Vinyl> vinyls = vinylDao.findByArtist(a);

        // Then
        assertEquals(1, vinyls.size());
    }

    @Test
    void whenFindingByCategory_shouldReturnInstancesRelatedToSameCategory() {
        // Given
        Category c = new Category(7, "Test", "Test");

        // When
        List<Vinyl> vinyls = vinylDao.findByCategory(c);

        // Then
        assertEquals(3, vinyls.size());
    }

    @Test
    void whenFindingByPublicationYear_shouldReturnInstancesWithParameterAsPublicationYear() {
        // Given
        int year = 2017;

        // When
        List<Vinyl> vinyls = vinylDao.findByPublicationYear(year);

        // Then
        assertEquals(2, vinyls.size());
    }

    @Test
    void whenFindingByDateRange_shouldReturnInstancesPublishedBetweenYearsAsParameters() {
        // Given
        int start = 2007;
        int end = 2012;

        // When
        List<Vinyl> vinyls = vinylDao.findByDateRange(start, end);

        // Then
        assertEquals(3, vinyls.size());
    }

    @Test
    void whenFindingByTitle_shouldReturnInstancesContainingParameter() {
        // Given
        String title = "born";

        // When
        List<Vinyl> vinyls = vinylDao.findByTitle(title);

        // Then
        assertEquals(1, vinyls.size());
    }

    @Test
    void whenAddingNewVinyl_shouldReturnInstanceWithId() {
        // Given
        Vinyl v = new Vinyl(
                "Hybrid Theory",
                new Artist(5, "Linkin Park"),
                2001,
                new Category(5, "Fantastique - Fantasy", "vinyl")
        );

        // When
        Vinyl vinyl = vinylDao.add(v);

        // Then
        assertTrue(vinyl.getId() > 5);
    }

    @Test
    void whenUpdatingVinyl_shouldReturnInstanceWithModification() {
        // Given
        Vinyl v = new Vinyl(
                2,
                "Meteora",
                new Artist(5, "Test"),
                2003,
                new Category(7, "TEst", "vinyl")
        );

        // When
        Vinyl vinyl = vinylDao.update(v);

        // Then
        assertAll(() -> {
            assertEquals("Meteora", vinyl.getTitle());
            assertEquals(2003, vinyl.getPublicationYear());
        });
    }

    @Test
    void whenDeletingVinyl_shouldRemoveInstanceQuietly() {
        // When
        Vinyl v = new Vinyl(
                5,
                "Test",
                new Artist(2, "Test"),
                0000,
                new Category(6, "TEst", "vinyl")
        );

        // Then
        assertDoesNotThrow(() -> vinylDao.delete(v));
    }
}