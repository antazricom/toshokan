package com.antazri.data.impl;

import com.antazri.data.AuthorDao;
import com.antazri.model.Author;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
class AuthorDaoIntegrationTest {

    @Autowired
    private AuthorDao authorDao;

    @Test
    void whenFindingById_shouldReturnObjectInstance() {
        // Given
        int id = 1;

        // When
        Optional<Author> author = authorDao.findById(id);

        // Then
        assertAll(() -> {
            assertTrue(author.isPresent());
            assertEquals("Murakami", author.get().getLastname());
        });
    }

    @Test
    void whenFindingByUnknownId_shouldReturnEmptyOptinal() {
        // Given
        int id = 100;

        // When
        Optional<Author> author = authorDao.findById(id);

        // Then
        assertTrue(author.isEmpty());
    }

    @Test
    void whenFindingByName_shouldReturnObjectsContainingParameter() {
        // Given
        String name = "tol";

        // When
        List<Author> authors = authorDao.findByName(name);

        // Then
        assertEquals(2, authors.size());
    }

    @Test
    void whenAddingInstance_shouldReturnObjectWithId() {
        // Given
        Author a = new Author("John", "Doe");

        // When
        Author author = authorDao.add(a);

        // Then
        assertTrue(author.getId() > 5);
    }

    @Test
    void whenUpdatingInstance_shouldReturnUpdatedObject() {
        // Given
        Author a = new Author("John", "Travolta");
        a.setId(3);

        // When
        Author author = authorDao.update(a);

        // Then
        assertEquals(author, authorDao.findById(3).get());
    }

    @Test
    void whenDeletingInstance_shouldRemoveObjectQuietly() {
        // When
        Author a = new Author("Risa", "Wataya");
        a.setId(2);

        // Then
        assertDoesNotThrow(() -> authorDao.delete(a));
    }
}