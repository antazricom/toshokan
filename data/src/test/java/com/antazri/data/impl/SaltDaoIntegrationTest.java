package com.antazri.data.impl;

import com.antazri.data.SaltDao;
import com.antazri.model.security.Salt;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
public class SaltDaoIntegrationTest {

    @Autowired
    private SaltDao saltDao;

    @Test
    void wheRequestingSaltByLoginId_shouldReturnSaltAsOptional() {
        // Given
        int loginId = 1;

        // When
        Optional<Salt> salt = saltDao.findByLoginId(loginId);

        // Then
        assertAll(() -> {
            assertTrue(salt.isPresent());
            assertEquals("salt", salt.get().getValue());
        });
    }

    @Test
    void wheRequestingSaltWithUnknownLoginId_shouldReturnEmptyOptional() {
        // Given
        int loginId = 2;

        // When
        Optional<Salt> salt = saltDao.findByLoginId(loginId);

        // Then
        assertTrue(salt.isEmpty());
    }
}
