package com.antazri.data.impl;

import com.antazri.data.ArtistDao;
import com.antazri.model.Artist;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
class ArtistDaoIntegrationTest {

    @Autowired
    private ArtistDao artistDao;

    @Test
    void whenGettingById_shouldReturnInstance() {
        // Given
        int id = 1;

        // When
        Optional<Artist> artist = artistDao.findById(id);

        // Then
        assertAll(() -> {
            assertTrue(artist.isPresent());
            assertEquals("U2", artist.get().getName());
        });
    }

    @Test
    void whenGettingByName_shouldReturnInstancesContainingParameter() {
        // Given
        String name = "o";

        // When
        List<Artist> artists = artistDao.findByName(name);

        // Then
        assertAll(() -> {
            assertFalse(artists.isEmpty());
            assertEquals(2, artists.size());
        });
    }

    @Test
    void whenFindAll_shouldReturnAllinstances() {
        // When
        List<Artist> artists = artistDao.findAll();

        // Then
        assertAll(() -> {
            assertFalse(artists.isEmpty());
            assertEquals(5, artists.size());
        });
    }

    @Test
    void whenAddingNewArtist_shouldReturnNewlyAddedWithId() {
        // Given
        Artist a = new Artist("Added");

        // When
        Artist artist = artistDao.add(a);

        // Then
        assertTrue(artist.getId() > 5);
    }

    @Test
    void whenUpdatingArtist_shouldReturnUpdatedInstance() {
        // Given
        Artist a = new Artist("Updated");
        a.setId(2);

        // When
        Artist artist = artistDao.update(a);

        // Then
        assertAll(() -> {
            assertEquals("Updated", artist.getName());
            assertEquals(a, artistDao.findById(2).get());
        });
    }

    @Test
    void whenDeletingInstance_shouldDeleteArtistQuietly() {
        // When
        Artist a = new Artist("A");
        a.setId(4);

        // Then
        assertDoesNotThrow(() -> artistDao.delete(a));
    }

}