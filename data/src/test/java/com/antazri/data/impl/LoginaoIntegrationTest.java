package com.antazri.data.impl;

import com.antazri.data.LoginDao;
import com.antazri.model.user.Login;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
public class LoginaoIntegrationTest {

    @Autowired
    private LoginDao loginDao;

    @Test
    void wheRequestingLoginByEmail_shouldReturnLoginAsOptional() {
        // Given
        String email = "root@test.com";

        // When
        Optional<Login> login = loginDao.findByEmail(email);

        // Then
        assertAll(() -> {
            assertTrue(login.isPresent());
            assertEquals("passwd", login.get().getPassword());
        });
    }

    @Test
    void wheRequestingLoginWithUnknownEmail_shouldReturnEmptyOptional() {
        // Given
        String email = "admin@test.com";

        // When
        Optional<Login> login = loginDao.findByEmail(email);

        // Then
        assertTrue(login.isEmpty());
    }

}
