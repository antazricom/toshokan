package com.antazri.data.impl;

import com.antazri.data.BookDao;
import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-data-test.xml"})
@Transactional
class BookDaoIntegrationTest {

    @Autowired
    private BookDao bookDao;

    @Test
    void whenFindingById_shouldReturnObjectAsOptional() {
        // Given
        int id = 1;

        // When
        Optional<Book> book = bookDao.findById(id);

        // Then
        assertAll(() -> {
            assertTrue(book.isPresent());
            assertEquals("2714447074", book.get().getIsbn());
        });
    }

    @Test
    void whenFindingByAuthor_shouldReturnInstancesRelatedToSameAuthor() {
        // Given
        Author a = new Author(1, "Test", "Test");

        // When
        List<Book> books = bookDao.findByAuthor(a);

        // Then
        assertEquals(3, books.size());
    }

    @Test
    void whenFindingByCategory_shouldReturnInstancesRelatedToSameCategory() {
        // Given
        Category c = new Category(5, "Test", "Test");

        // When
        List<Book> books = bookDao.findByCategory(c);

        // Then
        assertEquals(2, books.size());
    }

    @Test
    void whenFindingByIsbn_shouldReturnSingleInstanceWithIsbn() {
        // Given
        String isbn = "2253049417";

        // When
        Optional<Book> book = bookDao.findByIsbn(isbn);

        // Then
        assertAll(() -> {
            assertTrue(book.isPresent());
            assertEquals("Le Hobbit", book.get().getTitle());
        });
    }

    @Test
    void whenFindingByPublicationYear_shouldReturnInstancesWithParameterAsPublicationYear() {
        // Given
        int year = 1940;

        // When
        List<Book> books = bookDao.findByPublicationYear(year);

        // Then
        assertEquals(1, books.size());
    }

    @Test
    void whenFindingByDateRange_shouldReturnInstancesPublishedBetweenYearsAsParameters() {
        // Given
        int start = 1937;
        int end = 1940;

        // When
        List<Book> books = bookDao.findByDateRange(start, end);

        // Then
        assertEquals(2, books.size());
    }

    @Test
    void whenFindingByTitle_shouldReturnInstancesContainingParameter() {
        // Given
        String title = "le";

        // When
        List<Book> books = bookDao.findByTitle(title);

        // Then
        assertEquals(3, books.size());
    }

    @Test
    void whenAddingNewBook_shouldReturnInstanceWithId() {
        // Given
        Book b = new Book(
                "Le Seigneur des Anneaux 2 : Les Deux Tours",
                new Author(5, "John Ronald Reuel", "Tolkien"),
                1954,
                "978-5050023971",
                new Category(5, "Fantastique - Fantasy", "book")
        );

        // When
        Book book = bookDao.add(b);

        // Then
        assertTrue(book.getId() > 9);
    }

    @Test
    void whenUpdatingBook_shouldReturnInstanceWithModification() {
        // Given
        Book b = new Book(
                9,
                "Guerre et Paix : Tome 1",
                new Author(4, "Léon", "Tolstoï"),
                1954,
                "2253088978",
                new Category(3, "Littérature russe", "book")
        );

        // When
        Book book = bookDao.update(b);

        // Then
        assertAll(() -> {
            assertEquals("Guerre et Paix : Tome 1", book.getTitle());
            assertEquals("2253088978", book.getIsbn());
        });
    }

    @Test
    void whenDeletingBook_shouldRemoveInstanceQuietly() {
        // When
        Book b = new Book(
                8,
                "Test",
                new Author(2, "Test", "Test"),
                0000,
                "test",
                new Category(1, "TEst", "book")
        );

        // Then
        assertDoesNotThrow(() -> bookDao.delete(b));
    }
}