DELETE FROM public.book;
DELETE FROM public.vinyl;
DELETE FROM public.author;
DELETE FROM public.artist;
DELETE FROM public.category;
DELETE FROM public.login;
DELETE FROM public.salt;

INSERT INTO public.category
(id, name, type)
VALUES (1, 'Littérature japonaise', 'book'),
       (2, 'Littérature française', 'book'),
       (3, 'Littérature russe', 'book'),
       (4, 'Littérature américaine', 'book'),
       (5, 'Fantastique - Fantasy', 'book'),
       (6, 'Rap', 'vinyl'),
       (7, 'Pop - Rock', 'vinyl'),
       (8, 'Electro', 'vinyl'),
       (9, 'Metal', 'vinyl');

INSERT INTO public.author
(id, firstname, lastname)
VALUES (1, 'Haruki', 'Murakami'),
       (2, 'Risa', 'Wataya'),
       (3, 'Ernest', 'Hemingway'),
       (4, 'Leon', 'Tolstoï'),
       (5, 'John Ronald Reuel', 'Tolkien');

INSERT INTO public.book
(id, isbn, publication_year, title, author_id, category_id)
VALUES (1, '2714447074', '2009', '1Q84 Tome 1', 1, 1),
       (2, '2714449840', '2009', '1Q84 Tome 2', 1, 1),
       (3, '2714449859', '2010', '1Q84 Tome 3', 1, 1),
       (4, '2253049417', '1937', 'Le Hobbit', 5, 5),
       (5, '2266154117', '1954', 'Le Seigneur des Anneaux, Tome 1 : La Communauté de l''Anneau', 5, 5),
       (6, '2070364550', '1940', 'Pour qui sonne le glas', 3, 4),
       (7, '207036027X', '1929', 'L''adieu aux armes', 3, 4),
       (8, '2809700168', '2008', 'Appel du pied', 2, 1),
       (9, 'B074X5JZFJ', '2017', 'Guerre et Paix: Intégral', 4, 3);

INSERT INTO public.artist
(id, name)
VALUES (1, 'U2'),
       (2, 'IAM'),
       (3, 'John Mayer'),
       (4, 'Deftones'),
       (5, 'Linkin Park');

INSERT INTO public.vinyl
(id, title, publication_year, artist_id, category_id)
VALUES (1, 'The Joshua Tree', '2017', 1, 7),
       (2, 'Minutes to Midnight', '2007', 5, 7),
       (3, 'Koi No Yokan', '2008', 4, 9),
       (4, 'Born and Raised', '2011', 3, 7),
       (5, 'Yasuke', '2017', 2, 6);

INSERT INTO public.login
(id, email, password, role)
VALUES (1, 'root@test.com', 'passwd', 'ADMIN');

INSERT INTO public.salt
(loginid, value)
VALUES (1, 'salt');