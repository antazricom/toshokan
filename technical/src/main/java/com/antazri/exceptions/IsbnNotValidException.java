package com.antazri.exceptions;

public class IsbnNotValidException extends Exception {

    public IsbnNotValidException() {
    }

    public IsbnNotValidException(String message) {
        super(message);
    }

    public IsbnNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public IsbnNotValidException(Throwable cause) {
        super(cause);
    }

    protected IsbnNotValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
