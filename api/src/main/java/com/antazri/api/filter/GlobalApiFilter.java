package com.antazri.api.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class GlobalApiFilter implements Filter {

    private static final Logger logger = LogManager.getLogger(GlobalApiFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        logger.info("Requested URL: {}", servletRequest.getServletContext().toString());

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
