package com.antazri.api.filter;

import com.antazri.api.security.token.JwtHMAC512Provider;
import com.antazri.api.security.token.JwtProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.AccessControlException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AuthorizationFilter extends OncePerRequestFilter {

    private static final Logger logger = LogManager.getLogger(AuthorizationFilter.class);

    private static final String HEADER = "Authorization";
    private static final String AUTHORIZATION = "Bearer";

    private final JwtProvider jwtProvider;

    public AuthorizationFilter(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        logger.info("JwtAuthorizationFilter is watching you.");

        String authorization = request.getHeader(HEADER);

        if (authorization == null || !authorization.startsWith(AUTHORIZATION)) {
            logger.info("Unauthorized request: No token found");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            filterChain.doFilter(request, response);
            return;
        }

        Optional<UsernamePasswordAuthenticationToken> authenticate = this.authenticate(authorization);

        if (authenticate.isEmpty()) {
            logger.error("Authentication attempt failed: access denied");
            SecurityContextHolder.clearContext();
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            filterChain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authenticate.get());

        filterChain.doFilter(request, response);
    }

    private Optional<UsernamePasswordAuthenticationToken> authenticate(String authorization) {
        try {
            String token = authorization.split(" ")[1];
            Map<String, String> claims = this.jwtProvider.verify(token);

            if (claims.isEmpty()) {
                return Optional.empty();
            }

            return Optional.of(
                    new UsernamePasswordAuthenticationToken(
                            claims.get("login"),
                            null, List.of(
                            new SimpleGrantedAuthority(claims.get("role")))
                    )
            );
        } catch (AccessDeniedException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

}
