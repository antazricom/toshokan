package com.antazri.api.filter;

import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.BadRequestException;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebFilter(urlPatterns = "/*")
public class RequestParameterDecoderFilter extends OncePerRequestFilter {

    private static final Logger logger = LogManager.getLogger(RequestParameterDecoderFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        logger.debug("Init RequestParameterDecoderFilter");

        Map<String, String[]> parameterMap = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            if (entry.getKey().toLowerCase().endsWith("id")) {
                try {
                    logger.info(IdEncoder.decode(entry.getValue()[0]));
                } catch (DecodingException e) {
                    throw new ServletException("Error while decoding id in request uri");
                }
//                request.getParameterMap().put(entry.getKey(), new String[]{String.valueOf(IdEncoder.decode(entry.getValue()[0]))});
            }
        }

        filterChain.doFilter(request, response);
    }
}
