package com.antazri.api.security.auth.service;

import com.antazri.api.security.auth.LoginVerifier;
import com.antazri.api.security.auth.user.IdentifiedUser;
import com.antazri.api.security.auth.user.IdentifiedUserDetails;
import com.antazri.data.LoginDao;
import com.antazri.model.user.Login;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BasicUserService implements UserDetailsService {

    private static final Logger logger = LogManager.getLogger(BasicUserService.class);

    @Autowired
    private LoginDao loginDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Attempt to load user with login {}", username);

        LoginVerifier.check(username);

        Optional<Login> loginOpt = loginDao.findByEmail(username);

        if (loginOpt.isEmpty()) {
            logger.error("Authentication attempt failed: email {} not found", username);
            throw new UsernameNotFoundException("Authentication attempt failed: email not found");
        }

        return toExtendedUserDetails(loginOpt.get());
    }

    private IdentifiedUserDetails toExtendedUserDetails(Login login) {
        return new IdentifiedUser(
                login.getId(),
                login.getEmail(),
                login.getPassword(),
                List.of(new SimpleGrantedAuthority(login.getRole().getName()))
        );
    }
}
