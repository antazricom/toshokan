package com.antazri.api.security.auth;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

public class BasicPasswordEncoder implements PasswordEncoder {

    public static void main(String[] args) {
        String rawPsswd = "";
        PasswordEncoder passwordEncoder = new BasicPasswordEncoder();
        String hash = passwordEncoder.encode(rawPsswd);
        System.out.println(hash);
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return BCrypt.with(BCrypt.Version.VERSION_2Y).hashToString(10, String.valueOf(rawPassword).toCharArray());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        BCrypt.Result result = BCrypt.verifyer(BCrypt.Version.VERSION_2Y).verifyStrict(rawPassword.toString().getBytes(), encodedPassword.getBytes());
        return result.verified;
    }
}
