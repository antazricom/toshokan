package com.antazri.api.security.config;

import com.antazri.api.filter.AuthorizationFilter;
import com.antazri.api.security.auth.BasicPasswordEncoder;
import com.antazri.api.security.auth.service.BasicUserService;
import com.antazri.api.security.token.JwtHMAC512Provider;
import com.antazri.api.security.token.JwtProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable()
                .cors().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterAfter(new AuthorizationFilter(jwtProvider()), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/auth").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BasicPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new BasicUserService();
    }

    @Bean
    public JwtProvider jwtProvider() {
        return new JwtHMAC512Provider();
    }
}
