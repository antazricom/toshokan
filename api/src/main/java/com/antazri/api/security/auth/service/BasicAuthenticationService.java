package com.antazri.api.security.auth.service;

import com.antazri.api.security.auth.BasicPasswordEncoder;
import com.antazri.api.security.auth.LoginVerifier;
import com.antazri.api.security.auth.user.IdentifiedUserDetails;
import com.antazri.api.security.token.JwtHMAC512Provider;
import com.antazri.api.security.token.JwtProvider;
import com.antazri.data.SaltDao;
import com.antazri.model.security.Salt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class BasicAuthenticationService implements AuthenticationService {

    private static final Logger logger = LogManager.getLogger(BasicAuthenticationService.class);

    private final JwtProvider jwtProvider = new JwtHMAC512Provider();
    private final UserDetailsService userDetailsService;
    private final SaltDao saltDao;

    public BasicAuthenticationService(UserDetailsService userDetailsService, SaltDao saltDao) {
        this.userDetailsService = userDetailsService;
        this.saltDao = saltDao;
    }

    @Override
    public String auth(String login, String password) {
        logger.info("Authentication attempt with login {}", login);

        LoginVerifier.check(login);

        IdentifiedUserDetails userDetails = (IdentifiedUserDetails) userDetailsService.loadUserByUsername(login);

        Optional<Salt> saltOpt = saltDao.findByLoginId(userDetails.getId());

        if (saltOpt.isEmpty()) {
            logger.error("Server error: no salt found for login {}", login);
            throw new SecurityException("Authentication failed. Please request an Administrator.");
        }

        if (!verifyPassword(userDetails, saltOpt.get(), password)) {
            logger.error("Authentication denied for login {}", userDetails.getUsername());
            throw new AccessDeniedException("Access denied : email and password do not match");
        }

        return jwtProvider.generate(
                Map.of(
                        "login", userDetails.getUsername(),
                        "role", List.of(userDetails.getAuthorities()).get(0).toString()
                )
        );
    }

    private boolean verifyPassword(UserDetails userDetails, Salt salt, String clearPassword) {
        return new BasicPasswordEncoder()
                .matches(String.join("", clearPassword, salt.getValue()), userDetails.getPassword());
    }
}
