package com.antazri.api.security.token;

import org.springframework.security.access.AccessDeniedException;

import java.util.Map;

public interface JwtProvider {

    String generate(Map<String, String> claims);

    Map<String, String> verify(String token) throws AccessDeniedException;
}
