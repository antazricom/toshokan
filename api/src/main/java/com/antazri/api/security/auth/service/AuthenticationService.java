package com.antazri.api.security.auth.service;

@FunctionalInterface
public interface AuthenticationService {

    String auth(String login, String password);
}
