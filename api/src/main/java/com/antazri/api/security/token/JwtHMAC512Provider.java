package com.antazri.api.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtHMAC512Provider implements JwtProvider {

    private static final Logger logger = LogManager.getLogger(JwtHMAC512Provider.class);

    private static final String SECRET;
    private static final Algorithm ALGORITHM;
    private static final Long EXPIRATION_TIME;
    private static final String ISSUER;

    static {
        SECRET = System.getenv("TSKN_ID_SECRET");
        ALGORITHM = Algorithm.HMAC512(SECRET);
        EXPIRATION_TIME = 3600000L; // 1 hour
        ISSUER = "tskn";
    }

    @Override
    public String generate(Map<String, String> claims) {
        return JWT.create()
                .withIssuer(ISSUER)
                .withClaim("login", claims.get("login"))
                .withClaim("role", claims.get("role"))
                .withExpiresAt(new Date(new Date().getTime() + EXPIRATION_TIME))
                .sign(ALGORITHM);
    }

    @Override
    public Map<String, String> verify(String token) throws AccessDeniedException {
        JWTVerifier verifier = JWT.require(ALGORITHM).withIssuer(ISSUER).build();

        try {
            DecodedJWT decodedJWT = verifier.verify(token);
            return convertClaimsPayload(decodedJWT.getClaims());
        } catch (JWTVerificationException e) {
            logger.error("Error while decoding token");
            throw new AccessDeniedException("Token is not valid");
        }
    }

    private Map<String, String> convertClaimsPayload(Map<String, Claim> claims) {
        Map<String, String> payload = new HashMap<>();
        payload.put("login", claims.get("login").asString());
        payload.put("role", claims.get("role").asString());

        return payload;
    }
}
