package com.antazri.api.security.auth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Pattern;

public class LoginVerifier {

    private static final Logger logger = LogManager.getLogger(LoginVerifier.class);

    private static final Pattern emailPattern = Pattern.compile("^(.+)@(.+)$");

    public static void check(String email) {
        if (!emailPattern.matcher(email).matches()) {
            logger.error("Authentication attempt failed: login {} is not an email", email);
            throw new IllegalArgumentException("Authentication attempt failed: login is not an email");
        }

    }
}
