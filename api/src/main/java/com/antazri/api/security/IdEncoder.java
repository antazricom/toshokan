package com.antazri.api.security;

import at.favre.lib.idmask.Config;
import at.favre.lib.idmask.IdMask;
import at.favre.lib.idmask.IdMasks;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class IdEncoder {

    private static final Logger logger = LogManager.getLogger(IdEncoder.class);

    private static final String TSKN_SECRET;
    private static final IdMask<Long> mask;

    static {
        TSKN_SECRET = System.getenv("TSKN_ID_SECRET");
        mask = IdMasks.forLongIds(Config.builder(TSKN_SECRET.getBytes()).randomizedIds(true).build());
    }

    public static String encode(int id) {
        return mask.mask((long) id);
    }

    public static int decode(String encodedId) throws DecodingException {
        try {
            return mask.unmask(encodedId).intValue();
        } catch (RuntimeException e) {
            logger.error("Error while decoding id {}", encodedId);
            throw new DecodingException(e.getMessage());
        }
    }

    public static void main(String[] args) {
        final String encode = mask.mask(Long.parseLong("1"));
        System.out.println("encode: " + encode);
    }
}
