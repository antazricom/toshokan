package com.antazri.api.controller.response;

import java.util.HashMap;
import java.util.Map;

public class AppParameters {

    private final static String NAME = "Toshokan API";
    private final static String VERSION = "3.0.SNAPSHOT";
    private final static String RELEASE = "2022-03-27";

    public static Map<String, String> get() {
        Map<String, String> params = new HashMap<>();
        params.put("name", NAME);
        params.put("version", VERSION);
        params.put("release", RELEASE);

        return params;
    }
}
