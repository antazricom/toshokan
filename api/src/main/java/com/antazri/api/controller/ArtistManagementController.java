package com.antazri.api.controller;

import com.antazri.api.dto.ArtistDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.ArtistService;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/artist")
public class ArtistManagementController {

    private static final Logger logger = LogManager.getLogger(ArtistManagementController.class);

    private final ArtistService artistService;

    public ArtistManagementController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @GetMapping("{id}")
    public ResponseEntity<ArtistDto> getArtistDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get artist details {}", id);

        ArtistDto artist = artistService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No artist found"));

        return ResponseEntity.ok(artist);
    }

    @GetMapping
    public ResponseEntity<List<ArtistDto>> getAllArtists() {
        logger.info("Request get all artist");

        List<ArtistDto> artists = artistService.getAll();

        return ResponseEntity.ok(artists);
    }

    @PostMapping()
    public ResponseEntity<ArtistDto> postAddArtist(@RequestBody ArtistDto artist) {
        logger.info("Request add artist {}", artist);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PutMapping()
    public ResponseEntity<ArtistDto> putUpdateArtist(@RequestBody ArtistDto artist) {
        logger.info("Request update artist {}", artist);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deleteArtist(@RequestBody String id) {
        logger.info("Request delete artist {}", id);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
