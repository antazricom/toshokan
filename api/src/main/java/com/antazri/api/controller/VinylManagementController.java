package com.antazri.api.controller;

import com.antazri.api.dto.VinylDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.VinylService;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vinyl")
public class VinylManagementController {

    private static final Logger logger = LogManager.getLogger(VinylManagementController.class);

    private final VinylService vinylService;

    public VinylManagementController(VinylService vinylService) {
        this.vinylService = vinylService;
    }

    @GetMapping
    public ResponseEntity<List<VinylDto>> getAllVinyls() {
        logger.info("Request get all vinyls");

        List<VinylDto> vinyls = vinylService.getAll();

        return ResponseEntity.ok(vinyls);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VinylDto> getVinylDetails(@PathVariable("id") String id) {
        logger.info("Request get vinyl details {}", id);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @GetMapping("/title")
    public ResponseEntity<List<VinylDto>> getVinylsByTitle(@RequestBody String title) {
        logger.info("Request get vinyls by title {}", title);

        List<VinylDto> vinyls = vinylService.getByTitle(title);

        return ResponseEntity.ok(vinyls);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<VinylDto>> getVinylsByCategory(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get vinyls by category {}", id);

        List<VinylDto> vinyls = vinylService.getByCategory(IdEncoder.decode(id));

        return ResponseEntity.ok(vinyls);
    }

    @GetMapping("/artist/{id}")
    public ResponseEntity<List<VinylDto>> getVinylsByArtist(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get vinyls by artist {}", id);

        List<VinylDto> vinyls = vinylService.getByArtist(IdEncoder.decode(id));

        return ResponseEntity.ok(vinyls);
    }

    @PostMapping
    public ResponseEntity<VinylDto> postAddVinyl(@RequestBody VinylDto vinyl) {
        logger.info("Request add vinyl {}", vinyl);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PutMapping
    public ResponseEntity<VinylDto> putUpdateVinyl(@RequestBody VinylDto vinyl) {
        logger.info("Request update vinyl {}", vinyl);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteVinyl(@PathVariable("id") String id) {
        logger.info("Request delete vinyl {}", id);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
