package com.antazri.api.controller;

import com.antazri.api.dto.BookDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.AuthorService;
import com.antazri.api.services.BookService;
import com.antazri.api.services.CategoryService;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/books")
public class BookManagementController {

    private static final Logger logger = LogManager.getLogger(BookManagementController.class);

    private final BookService bookService;

    public BookManagementController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<BookDto>> getAllBooks() {
        logger.info("Request get all books");

        List<BookDto> books = bookService.getAll();

        return ResponseEntity.ok(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> getBookDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get book details {}", id);

        BookDto book = bookService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No book found"));

        return ResponseEntity.ok(book);
    }

    @GetMapping("/title")
    public ResponseEntity<List<BookDto>> getBooksByTitle(@RequestBody String title) {
        logger.info("Request get books by title {}", title);

        List<BookDto> books = bookService.getByTitle(title);

        return ResponseEntity.ok(books);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<BookDto>> getBooksByCategory(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get books by category {}", id);

        List<BookDto> books = bookService.getByCategory(IdEncoder.decode(id));

        return ResponseEntity.ok(books);
    }

    @GetMapping("/author/{id}")
    public ResponseEntity<List<BookDto>> getBooksByAuthor(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get books by author {}", id);

        List<BookDto> books = bookService.getByAuthor(IdEncoder.decode(id));

        return ResponseEntity.ok(books);
    }

    @PostMapping
    public ResponseEntity<BookDto> postAddBook(@RequestBody BookDto book) {
        logger.info("Request add book {}", book);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PutMapping
    public ResponseEntity<BookDto> putUpdateBook(@RequestBody BookDto book) {
        logger.info("Request update book {}", book);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteBook(@PathVariable("id") String id) {
        logger.info("Request delete book {}", id);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
