package com.antazri.api.controller;

import com.antazri.api.dto.AuthorDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.AuthorService;
import com.antazri.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/authors")
public class AuthorManagementController {

    private static final Logger logger = LogManager.getLogger(AuthorManagementController.class);

    private final AuthorService authorService;

    public AuthorManagementController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<List<AuthorDto>> getAllAuthors() {
        logger.info("Request all authors");

        List<AuthorDto> authors = authorService.getAll();

        return ResponseEntity.ok(authors);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDto> getAuthorDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request author details {}", id);

        AuthorDto author = authorService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No author found"));

        return ResponseEntity.ok(author);
    }

    @PostMapping()
    public ResponseEntity<AuthorDto> postAddAuthor(@RequestBody AuthorDto author) {
        logger.info("Request add author {}", author);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PutMapping()
    public ResponseEntity<AuthorDto> putUpdateAuthor(@RequestBody AuthorDto author) {
        logger.info("Request update author {}", author);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deleteAuthor(@RequestBody String id) {
        logger.info("Request delete author {}", id);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
