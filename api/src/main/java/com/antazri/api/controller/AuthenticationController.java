package com.antazri.api.controller;

import com.antazri.api.security.auth.AuthenticationRequest;
import com.antazri.api.security.auth.service.AuthenticationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private static final Logger logger = LogManager.getLogger(AuthenticationController.class);

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping
    public ResponseEntity<Map<String, String>> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
        if (!validRequest(authenticationRequest)) {
            logger.error("Attempt refused: bad request body");
            return buildErrorResponseEntity("Attempt refused: bad request body", HttpStatus.BAD_REQUEST);
        }

        logger.info("Authentication attempt {}", authenticationRequest);

        try {
            String accessToken = authenticationService.auth(
                    authenticationRequest.getLogin(),
                    authenticationRequest.getPassword()
            );

            return ResponseEntity.ok(Map.of("access_token", accessToken));
        } catch (IllegalArgumentException e) {
            logger.error("Attempt refused: bad request body");
            return buildErrorResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (UsernameNotFoundException e) {
            logger.error("Attempt refused: login not found");
            return buildErrorResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (AccessDeniedException e) {
            logger.error("Attempt refused: access denied");
            return buildErrorResponseEntity(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            logger.error("Attempt not completed: internal server error");
            return buildErrorResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean validRequest(AuthenticationRequest authenticationRequest) {
        return Stream.of(authenticationRequest, authenticationRequest.getLogin(), authenticationRequest.getPassword()).noneMatch(Objects::isNull)
                && !authenticationRequest.getLogin().isBlank()
                && !authenticationRequest.getPassword().isBlank();
    }

    private ResponseEntity<Map<String, String>> buildErrorResponseEntity(String message, HttpStatus status) {
        return new ResponseEntity<>(Map.of("error", message), status);
    }
}
