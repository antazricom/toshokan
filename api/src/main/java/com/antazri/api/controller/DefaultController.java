package com.antazri.api.controller;

import com.antazri.api.controller.response.AppParameters;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/")
public class DefaultController {

    @GetMapping
    public ResponseEntity getRootPage() {
        Map<String, String> params = AppParameters.get();
        params.put("deploy", "Toshokan API successfully started");

        return ResponseEntity.ok(params);
    }
}
