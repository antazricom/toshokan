package com.antazri.api.dto.mapper;

import com.antazri.api.dto.BookDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;

public final class BookMapper {

    private BookMapper() {
    }

    public static BookDto map(Book book) {
        if (book.getId() != null) {
            return new BookDto(
                    IdEncoder.encode(book.getId()),
                    book.getTitle(),
                    IdEncoder.encode(book.getAuthor().getId()),
                    book.getAuthor().getFirstname() != null ? book.getAuthor().getFirstname() : "",
                    book.getAuthor().getLastname() != null ? book.getAuthor().getLastname() : "",
                    book.getAuthor().getMiddlename() != null ? book.getAuthor().getMiddlename() : "",
                    book.getPublicationYear(),
                    book.getIsbn(),
                    IdEncoder.encode(book.getCategory().getId()),
                    book.getCategory().getName()
            );
        }

        return new BookDto(
                book.getTitle(),
                IdEncoder.encode(book.getAuthor().getId()),
                book.getAuthor().getFirstname() != null ? book.getAuthor().getFirstname() : "",
                book.getAuthor().getLastname() != null ? book.getAuthor().getLastname() : "",
                book.getAuthor().getMiddlename() != null ? book.getAuthor().getMiddlename() : "",
                book.getPublicationYear(),
                book.getIsbn(),
                IdEncoder.encode(book.getCategory().getId()),
                book.getCategory().getName()
        );
    }

    public static Book map(BookDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Book(
                    IdEncoder.decode(dto.getId()),
                    dto.getTitle(),
                    new Author(
                            IdEncoder.decode(dto.getAuthorId()),
                            dto.getAuthorFirstname() != null ? dto.getAuthorFirstname() : null,
                            dto.getAuthorLastname() != null ? dto.getAuthorLastname() : null,
                            dto.getAuthorMiddlename() != null ? dto.getAuthorMiddlename() : null),
                    dto.getPublicationYear(),
                    dto.getIsbn(),
                    new Category(IdEncoder.decode(dto.getCategoryId()), dto.getCategoryName())
            );
        }

        return new Book(
                dto.getTitle(),
                new Author(
                        IdEncoder.decode(dto.getAuthorId()),
                        dto.getAuthorFirstname() != null ? dto.getAuthorFirstname() : null,
                        dto.getAuthorLastname() != null ? dto.getAuthorLastname() : null,
                        dto.getAuthorMiddlename() != null ? dto.getAuthorMiddlename() : null),
                dto.getPublicationYear(),
                dto.getIsbn(),
                new Category(IdEncoder.decode(dto.getCategoryId()), dto.getCategoryName())
        );
    }
}
