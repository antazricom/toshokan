package com.antazri.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class VinylDto implements Serializable {

    private String id;
    private String title;
    private String artistId;
    private String artistName;
    private int publicationYear;
    private String categoryId;
    private String categoryName;

    public VinylDto() {
    }

    public VinylDto(String title, String artistId, String artistName, int publicationYear,
                    String categoryId, String categoryName) {
        this.title = title;
        this.artistId = artistId;
        this.artistName = artistName;
        this.publicationYear = publicationYear;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public VinylDto(String id, String title, String artistId, String artistName, int publicationYear,
                    String categoryId, String categoryName) {
        this.id = id;
        this.title = title;
        this.artistId = artistId;
        this.artistName = artistName;
        this.publicationYear = publicationYear;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylDto vinylDto = (VinylDto) o;
        return publicationYear == vinylDto.publicationYear
                && Objects.equals(id, vinylDto.id)
                && Objects.equals(title, vinylDto.title)
                && Objects.equals(artistId, vinylDto.artistId)
                && Objects.equals(artistName, vinylDto.artistName)
                && Objects.equals(categoryId, vinylDto.categoryId)
                && Objects.equals(categoryName, vinylDto.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, artistId, artistName, publicationYear, categoryId, categoryName);
    }

    @Override
    public String toString() {
        return "VinylDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", artistId='" + artistId + '\'' +
                ", artistName='" + artistName + '\'' +
                ", publicationYear=" + publicationYear +
                ", categoryId='" + categoryId + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
