package com.antazri.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class BookDto implements Serializable {

    private String id;
    private String title;
    private String authorId;
    private String authorFirstname;
    private String authorMiddlename;
    private String authorLastname;
    private int publicationYear;
    private String isbn;
    private String categoryId;
    private String categoryName;

    public BookDto() {
    }

    public BookDto(String title, String authorId, String authorFirstname, String authorMiddlename,
                   String authorLastname, int publicationYear, String isbn, String categoryId, String categoryName) {
        this.title = title;
        this.authorId = authorId;
        this.authorFirstname = authorFirstname;
        this.authorMiddlename = authorMiddlename;
        this.authorLastname = authorLastname;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public BookDto(String id, String title, String authorId, String authorFirstname, String authorLastname,
                   String authorMiddlename, int publicationYear, String isbn, String categoryId, String categoryName) {
        this.id = id;
        this.title = title;
        this.authorId = authorId;
        this.authorFirstname = authorFirstname;
        this.authorMiddlename = authorMiddlename;
        this.authorLastname = authorLastname;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getAuthorFirstname() {
        return authorFirstname;
    }

    public void setAuthorFirstname(String authorFirstname) {
        this.authorFirstname = authorFirstname;
    }

    public String getAuthorMiddlename() {
        return authorMiddlename;
    }

    public void setAuthorMiddlename(String authorMiddlename) {
        this.authorMiddlename = authorMiddlename;
    }

    public String getAuthorLastname() {
        return authorLastname;
    }

    public void setAuthorLastname(String authorLastname) {
        this.authorLastname = authorLastname;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDto bookDto = (BookDto) o;
        return publicationYear == bookDto.publicationYear
                && Objects.equals(id, bookDto.id)
                && Objects.equals(title, bookDto.title)
                && Objects.equals(authorId, bookDto.authorId)
                && Objects.equals(authorFirstname, bookDto.authorFirstname)
                && Objects.equals(authorMiddlename, bookDto.authorMiddlename)
                && Objects.equals(authorLastname, bookDto.authorLastname)
                && Objects.equals(isbn, bookDto.isbn)
                && Objects.equals(categoryId, bookDto.categoryId)
                && Objects.equals(categoryName, bookDto.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, authorId, authorFirstname, authorMiddlename, authorLastname,
                publicationYear, isbn, categoryId, categoryName);
    }

    @Override
    public String toString() {
        return "BookDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", authorId='" + authorId + '\'' +
                ", authorFirstname='" + authorFirstname + '\'' +
                ", authorMiddlename='" + authorMiddlename + '\'' +
                ", authorLastname='" + authorLastname + '\'' +
                ", publicationYear=" + publicationYear +
                ", isbn='" + isbn + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
