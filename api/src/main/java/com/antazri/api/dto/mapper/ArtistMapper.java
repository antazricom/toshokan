package com.antazri.api.dto.mapper;

import com.antazri.api.dto.ArtistDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Artist;

public final class ArtistMapper {

    private ArtistMapper() {
    }

    public static ArtistDto map(Artist artist) {
        if (artist.getId() != null) {
            return new ArtistDto(
                    IdEncoder.encode(artist.getId()),
                    artist.getName()
            );
        }

        return new ArtistDto(artist.getName());
    }

    public static Artist map(ArtistDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Artist(
                    IdEncoder.decode(dto.getId()),
                    dto.getName()
            );
        }

        return new Artist(dto.getName());
    }
}
