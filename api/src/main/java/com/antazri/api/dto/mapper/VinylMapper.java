package com.antazri.api.dto.mapper;

import com.antazri.api.dto.VinylDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;

public final class VinylMapper {

    private VinylMapper() {
    }

    public static VinylDto map(Vinyl vinyl) {
        if (vinyl.getId() != null) {
            return new VinylDto(
                    IdEncoder.encode(vinyl.getId()),
                    vinyl.getTitle(),
                    IdEncoder.encode(vinyl.getArtist().getId()),
                    vinyl.getArtist().getName(),
                    vinyl.getPublicationYear(),
                    IdEncoder.encode(vinyl.getCategory().getId()),
                    vinyl.getCategory().getName()
            );
        }

        return new VinylDto(
                vinyl.getTitle(),
                IdEncoder.encode(vinyl.getArtist().getId()),
                vinyl.getArtist().getName(),
                vinyl.getPublicationYear(),
                IdEncoder.encode(vinyl.getCategory().getId()),
                vinyl.getCategory().getName()
        );
    }

    public static Vinyl map(VinylDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Vinyl(
                    IdEncoder.decode(dto.getId()),
                    dto.getTitle(),
                    new Artist(IdEncoder.decode(dto.getArtistId()), dto.getArtistName()),
                    dto.getPublicationYear(),
                    new Category(IdEncoder.decode(dto.getCategoryId()), dto.getCategoryName())
            );
        }

        return new Vinyl(
                dto.getTitle(),
                new Artist(IdEncoder.decode(dto.getArtistId()), dto.getArtistName()),
                dto.getPublicationYear(),
                new Category(IdEncoder.decode(dto.getCategoryId()), dto.getCategoryName())
        );
    }
}
