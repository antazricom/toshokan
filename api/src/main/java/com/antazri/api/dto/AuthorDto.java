package com.antazri.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class AuthorDto implements Serializable {

    private String id;
    private String firstname;
    private String middlename;
    private String lastname;

    public AuthorDto() {
    }

    public AuthorDto(String firstname, String middlename, String lastname) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
    }

    public AuthorDto(String id, String firstname, String middlename, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDto authorDto = (AuthorDto) o;
        return Objects.equals(id, authorDto.id)
                && Objects.equals(firstname, authorDto.firstname)
                && Objects.equals(middlename, authorDto.middlename)
                && Objects.equals(lastname, authorDto.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, middlename, lastname);
    }

    @Override
    public String toString() {
        return "AuthorDto{" +
                "id='" + id + '\'' +
                ", firstname='" + firstname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
