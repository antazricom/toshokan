package com.antazri.api.dto.mapper;

import com.antazri.api.dto.CategoryDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Category;

public final class CategoryMapper {

    private CategoryMapper() {
    }

    public static CategoryDto map(Category category) {
        if (category.getId() != null) {
            return new CategoryDto(
                    IdEncoder.encode(category.getId()),
                    category.getName(),
                    category.getType()
            );
        }

        return new CategoryDto(category.getName(), category.getType());
    }

    public static Category map(CategoryDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Category(
                    IdEncoder.decode(dto.getId()),
                    dto.getName(),
                    dto.getType()
            );
        }

        return new Category(dto.getName(), dto.getType());
    }
}
