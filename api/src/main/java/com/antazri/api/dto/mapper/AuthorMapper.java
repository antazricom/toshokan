package com.antazri.api.dto.mapper;

import com.antazri.api.dto.AuthorDto;
import com.antazri.api.security.IdEncoder;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Author;

public final class AuthorMapper {

    private AuthorMapper() {
    }

    public static AuthorDto map(Author author) {
        return new AuthorDto(
                IdEncoder.encode(author.getId()),
                author.getFirstname(),
                author.getMiddlename() != null ? author.getMiddlename() : "",
                author.getLastname()
        );
    }

    public static Author map(AuthorDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Author(
                    IdEncoder.decode(dto.getId()),
                    dto.getFirstname(),
                    dto.getMiddlename() != null ? dto.getMiddlename() : null,
                    dto.getLastname()
            );
        }

        return new Author(
                dto.getFirstname(),
                dto.getMiddlename() != null ? dto.getMiddlename() : null,
                dto.getLastname()
        );
    }
}
