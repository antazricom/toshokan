package com.antazri.api.services.impl;

import com.antazri.api.dto.CategoryDto;
import com.antazri.api.dto.mapper.CategoryMapper;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.CategoryService;
import com.antazri.data.CategoryDao;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Qualifier("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private static final Logger logger = LogManager.getLogger(CategoryServiceImpl.class);

    private final CategoryDao categoryDao;

    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Optional<CategoryDto> getDetails(int id) {
        final Optional<Category> category = categoryDao.findById(id);

        if (category.isEmpty()) {
            logger.info("Category not found with id {}", id);
            return Optional.empty();
        }

        return category.map(CategoryMapper::map);
    }

    @Override
    public List<CategoryDto> getAll() {
        return categoryDao.findAll().stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDto add(CategoryDto category) throws DecodingException {
        if (!validAddRequest(category)) {
            logger.error("Category add request is not valid");
            throw new IllegalArgumentException("Category add request is not valid");
        }

        logger.info("Add Category {}", category);
        Category categoryDb = CategoryMapper.map(category);

        return CategoryMapper.map(categoryDao.add(categoryDb));
    }

    private boolean validAddRequest(CategoryDto category) {
        return category != null
                && !category.getName().isBlank()
                && !category.getType().isBlank();
    }

    @Override
    public CategoryDto update(CategoryDto category) throws DecodingException {
        if (!validUpdateRequest(category)) {
            logger.error("Category update request is not valid");
            throw new IllegalArgumentException("Category update request is not valid");
        }

        logger.info("Update Category {}", category);
        Category categoryDb = CategoryMapper.map(category);

        return CategoryMapper.map(categoryDao.update(categoryDb));
    }

    private boolean validUpdateRequest(CategoryDto category) throws DecodingException {
        return !category.getId().isBlank()
                && !category.getName().isBlank()
                && !category.getType().isBlank()
                && categoryDao.findById(IdEncoder.decode(category.getId())).isPresent();
    }

    @Override
    public boolean remove(int id) {
        Optional<Category> category = categoryDao.findById(id);

        if (category.isEmpty()) {
            logger.error("No category find with id {}", id);
            throw new IllegalArgumentException("No category found");
        }

        logger.info("Remove Category {}", category.get());
        categoryDao.delete(category.get());
        return true;
    }
}
