package com.antazri.api.services;

import com.antazri.api.dto.BookDto;

import java.util.List;
import java.util.Optional;

public interface BookService extends BasicService<BookDto> {

    Optional<BookDto> getDetails(int id);

    Optional<BookDto> getByIsbn(String isbn);

    List<BookDto> getByTitle(String title);

    List<BookDto> getByPublicationYear(int year);

    List<BookDto> getByAuthor(int authorId);

    List<BookDto> getByCategory(int categoryId);
}
