package com.antazri.api.services.impl;

import com.antazri.api.dto.VinylDto;
import com.antazri.api.dto.mapper.VinylMapper;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.VinylService;
import com.antazri.data.ArtistDao;
import com.antazri.data.CategoryDao;
import com.antazri.data.VinylDao;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("vinylService")
public class VinylServiceImpl implements VinylService {

    private static final Logger logger = LogManager.getLogger(VinylServiceImpl.class);

    private final VinylDao vinylDao;
    private final ArtistDao artistDao;
    private final CategoryDao categoryDao;

    public VinylServiceImpl(VinylDao vinylDao, ArtistDao artistDao, CategoryDao categoryDao) {
        this.vinylDao = vinylDao;
        this.artistDao = artistDao;
        this.categoryDao = categoryDao;
    }

    @Override
    public Optional<VinylDto> getDetails(int id) {
        final Optional<Vinyl> vinyl = vinylDao.findById(id);

        if (vinyl.isEmpty()) {
            logger.info("No Vinyl found with id {}", id);
            return Optional.empty();
        }

        return vinyl.map(VinylMapper::map);
    }

    @Override
    public List<VinylDto> getByTitle(String title) {
        final Collection<Vinyl> vinylsDb = vinylDao.findByTitle(title.trim());

        if (vinylsDb.isEmpty()) {
            logger.info("No vinyl found with title {}", title);
            return Collections.emptyList();
        }

        return vinylsDb.stream().map(VinylMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByArtist(int artistId) {
        Optional<Artist> artistOpt = artistDao.findById(artistId);

        if (artistOpt.isEmpty()) {
            logger.error("No artist found with id {}", artistId);
            return Collections.emptyList();
        }

        return vinylDao.findByArtist(artistOpt.get()).stream().map(VinylMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByPublicationYear(int year) {
        return vinylDao.findByPublicationYear(year).stream().map(VinylMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByCategory(int categoryId) {
        Optional<Category> categoryOpt = categoryDao.findById(categoryId);

        if (categoryOpt.isEmpty()) {
            logger.error("No category found with id {}", categoryId);
            return Collections.emptyList();
        }

        return vinylDao.findByCategory(categoryOpt.get()).stream().map(VinylMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getAll() {
        return vinylDao.findAll().stream()
                .map(VinylMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public VinylDto add(VinylDto vinyl) throws DecodingException {
        if (!validAddRequest(vinyl)) {
            logger.error("Vinyl to add is not valid");
            throw new IllegalArgumentException("Vinyl to add is not valid");
        }

        logger.info("Add Vinyl {}", vinyl);
        Vinyl vinylDb = VinylMapper.map(vinyl);

        return VinylMapper.map(vinylDao.add(vinylDb));
    }

    private boolean validAddRequest(VinylDto vinyl) throws DecodingException {
        return Stream.of(vinyl.getTitle(), vinyl.getArtistId(), vinyl.getCategoryId(), vinyl.getPublicationYear())
                .noneMatch(Objects::isNull)
                && artistDao.findById(IdEncoder.decode(vinyl.getArtistId())).isPresent()
                && categoryDao.findById(IdEncoder.decode(vinyl.getCategoryId())).isPresent()
                && (vinyl.getPublicationYear() > 0 && vinyl.getPublicationYear() <= LocalDate.now().getYear());
    }

    @Override
    public VinylDto update(VinylDto vinyl) throws DecodingException {
        if (!validUpdateRequest(vinyl)) {
            logger.error("Vinyl to update is not valid");
            throw new IllegalArgumentException("Vinyl to update is not valid");
        }

        logger.info("Update Vinyl {}", vinyl);
        Vinyl vinylDb = VinylMapper.map(vinyl);

        return VinylMapper.map(vinylDao.update(vinylDb));
    }

    private boolean validUpdateRequest(VinylDto vinyl) throws DecodingException {
        return Stream.of(vinyl.getId(), vinyl.getTitle(), vinyl.getArtistId(), vinyl.getCategoryId(), vinyl.getPublicationYear())
                .noneMatch(Objects::isNull)
                && vinylDao.findById(IdEncoder.decode(vinyl.getId())).isPresent()
                && artistDao.findById(IdEncoder.decode(vinyl.getArtistId())).isPresent()
                && categoryDao.findById(IdEncoder.decode(vinyl.getCategoryId())).isPresent()
                && (vinyl.getPublicationYear() > 0 && vinyl.getPublicationYear() <= LocalDate.now().getYear());
    }

    @Override
    public boolean remove(int id) {
        final Optional<Vinyl> vinyl = vinylDao.findById(id);

        if (vinyl.isEmpty()) {
            logger.error("No vinyl find with id {}", id);
            throw new IllegalArgumentException("No vinyl found");
        }

        logger.info("Remove Vinyl {}", vinyl.get());
        vinylDao.delete(vinyl.get());
        return true;
    }
}
