package com.antazri.api.services.impl;

import com.antazri.api.dto.AuthorDto;
import com.antazri.api.dto.mapper.AuthorMapper;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.AuthorService;
import com.antazri.data.AuthorDao;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Author;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("authorService")
public class AuthorServiceImpl implements AuthorService {

    private static final Logger logger = LogManager.getLogger(AuthorServiceImpl.class);

    private final AuthorDao authorDao;

    public AuthorServiceImpl(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public Optional<AuthorDto> getDetails(int id) {
        final Optional<Author> author = authorDao.findById(id);


        return author.map(AuthorMapper::map);
    }

    @Override
    public List<AuthorDto> searchByName(String name) {
        return authorDao.findByName(name).stream().map(AuthorMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<AuthorDto> getAll() {
        return authorDao.findAll().stream().map(AuthorMapper::map).collect(Collectors.toList());
    }

    @Override
    public AuthorDto add(AuthorDto author) throws DecodingException {
        if (!validAddRequest(author)) {
            logger.error("Author {} from request is not valid", author);
            throw new IllegalArgumentException("Author from request is not valid");
        }

        logger.info("Add {}", author);
        Author authorDb = AuthorMapper.map(author);

        return AuthorMapper.map(authorDao.add(authorDb));
    }

    private boolean validAddRequest(AuthorDto author) {
        return Stream.of(author.getFirstname(), author.getLastname()).noneMatch(Objects::isNull)
                && Stream.of(author.getFirstname(), author.getLastname()).noneMatch(String::isBlank);
    }

    @Override
    public AuthorDto update(AuthorDto author) throws DecodingException {
        if (!validUpdateRequest(author)) {
            logger.error("Update for Author {} not valid", author);
            throw new IllegalArgumentException("Author update not valid");
        }

        logger.info("Update {}", author);
        Author dbAuthor = AuthorMapper.map(author);

        return AuthorMapper.map(dbAuthor);
    }

    private boolean validUpdateRequest(AuthorDto author) throws DecodingException {
        return Stream.of(author.getId(), author.getFirstname(), author.getLastname()).anyMatch(Objects::isNull)
                && Stream.of(author.getId(), author.getFirstname(), author.getLastname()).anyMatch(String::isBlank)
                && authorDao.findById(IdEncoder.decode(author.getId())).isPresent();
    }

    @Override
    public boolean remove(int id) {
        Optional<Author> author = authorDao.findById(id);
        if (author.isEmpty()) {
            logger.error("Author not found with id: {}", id);
            throw new IllegalArgumentException("Author not found with id: " + id);
        }

        logger.info("Remove Author {}", author.get());
        authorDao.delete(author.get());
        return true;
    }
}
