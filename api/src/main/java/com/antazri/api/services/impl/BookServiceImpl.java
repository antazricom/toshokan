package com.antazri.api.services.impl;

import com.antazri.api.dto.BookDto;
import com.antazri.api.dto.mapper.BookMapper;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.BookService;
import com.antazri.data.AuthorDao;
import com.antazri.data.BookDao;
import com.antazri.data.CategoryDao;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("bookService")
public class BookServiceImpl implements BookService {

    private static final Logger logger = LogManager.getLogger(BookServiceImpl.class);
    private static final Pattern ISBN = Pattern.compile("(?=(?:\\D*\\d){10}(?:(?:\\D@*\\d){3})?$)");
    private final BookDao bookDao;
    private final CategoryDao categoryDao;
    private final AuthorDao authorDao;

    public BookServiceImpl(BookDao bookDao, CategoryDao categoryDao, AuthorDao authorDao) {
        this.bookDao = bookDao;
        this.categoryDao = categoryDao;
        this.authorDao = authorDao;
    }

    @Override
    public Optional<BookDto> getDetails(int id) {
        final Optional<Book> dbBook = bookDao.findById(id);

        if (dbBook.isEmpty()) {
            logger.info("Book not found with id {}", id);
            return Optional.empty();
        }

        logger.info("Get book {} details", id);

        return dbBook.map(BookMapper::map);
    }

    @Override
    public Optional<BookDto> getByIsbn(String isbn) {
        if (ISBN.matcher(isbn).find()) {
            logger.info("Get book details for ISBN {}", isbn);
            final Optional<Book> bookDb = bookDao.findByIsbn(isbn);

            if (bookDb.isEmpty()) {
                return Optional.empty();
            } else {
                return bookDb.map(BookMapper::map);
            }
        }

        logger.error("ISBN format is not valid");
        throw new IllegalArgumentException("ISB format is not valid");
    }

    @Override
    public List<BookDto> getByTitle(String title) {
        final List<Book> dbBooks = new ArrayList<>(bookDao.findByTitle(title.trim()));
        return dbBooks.stream().map(BookMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByPublicationYear(int year) {
        final List<Book> dbBooks = new ArrayList<>(bookDao.findByPublicationYear(year));
        return dbBooks.stream().map(BookMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByAuthor(int authorId) {
        Optional<Author> authorOpt = authorDao.findById(authorId);

        if (authorOpt.isEmpty()) {
            logger.error("No author found with id {}", authorId);
            return Collections.emptyList();
        }

        return bookDao.findByAuthor(authorOpt.get()).stream().map(BookMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByCategory(int categoryId) {
        Optional<Category> categoryOpt = categoryDao.findById(categoryId);

        if (categoryOpt.isEmpty()) {
            logger.error("No Category found with id {}", categoryId);
            return Collections.emptyList();
        }

        return bookDao.findByCategory(categoryOpt.get()).stream().map(BookMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getAll() {
        return bookDao.findAll().stream()
                .map(BookMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public BookDto add(BookDto book) throws DecodingException {
        if (!validAddBookRequest(book)) {
            logger.error("Request to add new book is not valid");
            throw new IllegalArgumentException("Request to add new book is not valid");
        }

        logger.info("Add Book {}", book);
        Book added = BookMapper.map(book);

        return BookMapper.map(bookDao.add(added));
    }

    private boolean validAddBookRequest(BookDto book) {
        return Stream.of(book.getAuthorId(), book.getCategoryId(), book.getIsbn(), book.getPublicationYear(), book.getTitle())
                .noneMatch(Objects::nonNull)
                && ISBN.matcher(book.getIsbn()).find()
                && !book.getTitle().isBlank()
                && (book.getPublicationYear() > 0 && book.getPublicationYear() <= LocalDate.now().getYear());
    }

    @Override
    public BookDto update(BookDto book) throws DecodingException {
        if (!validUpdateBookRequest(book)) {
            logger.error("Request to add new book is not valid");
            throw new IllegalArgumentException("Request to add new book is not valid");
        }

        logger.info("Update Book {}", book);
        Book updated = BookMapper.map(book);

        return BookMapper.map(bookDao.update(updated));
    }

    private boolean validUpdateBookRequest(BookDto book) throws DecodingException {
        return Stream.of(book.getAuthorId(), book.getCategoryId(), book.getIsbn(), book.getPublicationYear(), book.getTitle())
                .noneMatch(Objects::nonNull)
                && ISBN.matcher(book.getIsbn()).find()
                && !book.getTitle().isBlank()
                && (book.getPublicationYear() > 0 && book.getPublicationYear() <= LocalDate.now().getYear())
                && bookDao.findById(IdEncoder.decode(book.getId())).isPresent();
    }

    @Override
    public boolean remove(int id) {
        final Optional<Book> bookOpt = bookDao.findById(id);

        if (bookOpt.isEmpty()) {
            logger.error("Book not found with id: {}", id);
            throw new IllegalArgumentException("Book not found with id: " + id);
        }

        bookDao.delete(bookOpt.get());
        logger.info("Book {} has been removed", bookOpt.get());
        return true;
    }
}
