package com.antazri.api.services;

import com.antazri.api.dto.CategoryDto;

import java.util.Optional;

public interface CategoryService extends BasicService<CategoryDto> {

    Optional<CategoryDto> getDetails(int id);

}
