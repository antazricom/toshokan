package com.antazri.api.services;

import com.antazri.exceptions.DecodingException;

import java.util.List;

public interface BasicService<T> {

    List<T> getAll();

    T add(T entity) throws DecodingException;

    T update(T entity) throws DecodingException;

    boolean remove(int id);
}
