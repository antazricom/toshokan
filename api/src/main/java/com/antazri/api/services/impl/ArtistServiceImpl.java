package com.antazri.api.services.impl;

import com.antazri.api.dto.ArtistDto;
import com.antazri.api.dto.mapper.ArtistMapper;
import com.antazri.api.security.IdEncoder;
import com.antazri.api.services.ArtistService;
import com.antazri.data.ArtistDao;
import com.antazri.exceptions.DecodingException;
import com.antazri.model.Artist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("artistService")
public class ArtistServiceImpl implements ArtistService {

    private final static Logger logger = LogManager.getLogger(ArtistServiceImpl.class);

    private final ArtistDao artistDao;

    public ArtistServiceImpl(ArtistDao artistDao) {
        this.artistDao = artistDao;
    }

    @Override
    public Optional<ArtistDto> getDetails(int id) {
        final Optional<Artist> artist = artistDao.findById(id);

        if (artist.isEmpty()) {
            logger.error("Artist not found with id {}", id);
            return Optional.empty();
        }

        return artist.map(ArtistMapper::map);
    }

    @Override
    public List<ArtistDto> searchByName(String name) {
        final List<Artist> artists = artistDao.findByName(name.trim());

        if (!artists.isEmpty()) {
            return artists.stream().map(ArtistMapper::map).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<ArtistDto> getAll() {
        return artistDao.findAll().stream()
                .map(ArtistMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public ArtistDto add(ArtistDto artist) throws DecodingException {
        if (!validAddRequest(artist)) {
            logger.error("Artist.name not valid: {}", artist);
            throw new IllegalArgumentException("Artist name is not valid");
        }

        logger.info("Add {}", artist);
        Artist artistDb = ArtistMapper.map(artist);

        return ArtistMapper.map(artistDao.add(artistDb));
    }

    private boolean validAddRequest(ArtistDto artist) {
        return !artist.getName().isBlank();
    }

    @Override
    public ArtistDto update(ArtistDto artist) throws DecodingException {
        if (!validUpdateRequest(artist)) {
            logger.error("Artist to update is not valid: {}", artist);
            throw new IllegalArgumentException("Artist to update is not valid");
        }

        logger.info("Update {}", artist);
        Artist artistDb = ArtistMapper.map(artist);

        return ArtistMapper.map(artistDao.update(artistDb));
    }

    private boolean validUpdateRequest(ArtistDto artist) throws DecodingException {
        return Stream.of(artist.getId(), artist.getName()).noneMatch(Objects::isNull)
                && Stream.of(artist.getId(), artist.getName()).noneMatch(String::isBlank)
                && artistDao.findById(IdEncoder.decode(artist.getId())).isPresent();
    }

    @Override
    public boolean remove(int id) {
        final Optional<Artist> artist = artistDao.findById(id);
        if (artist.isEmpty()) {
            logger.error("Artist not found with id: {}", id);
            throw new IllegalArgumentException("Artist not found with id: " + id);
        }

        logger.info("Remove Artist {}", artist.get());
        artistDao.delete(artist.get());
        return true;
    }
}
