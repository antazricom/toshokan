package com.antazri.api.services;

import com.antazri.api.dto.ArtistDto;

import java.util.List;
import java.util.Optional;

public interface ArtistService extends BasicService<ArtistDto> {

    Optional<ArtistDto> getDetails(int id);

    List<ArtistDto> searchByName(String name);
}
