package com.antazri.api.services;


import com.antazri.api.dto.AuthorDto;

import java.util.List;
import java.util.Optional;

public interface AuthorService extends BasicService<AuthorDto> {

    Optional<AuthorDto> getDetails(int id);

    List<AuthorDto> searchByName(String name);

}
