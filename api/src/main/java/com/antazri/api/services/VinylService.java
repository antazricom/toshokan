package com.antazri.api.services;

import com.antazri.api.dto.VinylDto;

import java.util.List;
import java.util.Optional;

public interface VinylService extends BasicService<VinylDto> {

    Optional<VinylDto> getDetails(int id);

    List<VinylDto> getByArtist(int artistId);

    List<VinylDto> getByPublicationYear(int year);

    List<VinylDto> getByCategory(int categoryId);

    List<VinylDto> getByTitle(String title);
}
