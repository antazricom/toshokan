package com.antazri.api.security.token;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class JwtHMAC512ProviderTest {

    private final JwtProvider provider = new JwtHMAC512Provider();

    @Test
    @Order(1)
    void whenGeneratingJwt_shouldReturnTokenAsString() {
        // Given
        Map<String, String> claims = new HashMap<>();
        claims.put("login", "test@test.com");
        claims.put("role", "ROLE_ADMIN");

        // When
        String token = provider.generate(claims);

        // Then
        assertAll(() -> {
            assertFalse(token.isEmpty());
            assertTrue(token.startsWith("ey"));
        });
    }

    @Test
    @Order(2)
    void whenVerifyingJwt_shouldReturnClaims() {
        // Given
        Map<String, String> claims = new HashMap<>();
        claims.put("login", "test@test.com");
        claims.put("role", "ROLE_ADMIN");
        String token = provider.generate(claims);

        // When
        Map<String, String> payload = provider.verify(token);

        // Then
        assertAll(() -> {
            assertTrue(payload.containsKey("login"));
            assertEquals("test@test.com", payload.get("login"));
            assertTrue(payload.containsKey("role"));
            assertEquals("ROLE_ADMIN", payload.get("role"));
        });
    }
}