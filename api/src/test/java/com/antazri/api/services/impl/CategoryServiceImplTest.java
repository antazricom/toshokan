package com.antazri.api.services.impl;

import com.antazri.api.dto.CategoryDto;
import com.antazri.api.services.CategoryService;
import com.antazri.data.CategoryDao;
import com.antazri.data.impl.CategoryDaoImpl;
import com.antazri.model.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-api-test.xml"})
class CategoryServiceImplTest {

    private final CategoryDao categoryDao = mock(CategoryDaoImpl.class);
    private final CategoryService categoryService = new CategoryServiceImpl(categoryDao);

    @Test
    void whenGettingDetailsWithNotFoundId_shouldReturnEmptyOptional() {
        // Given
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<CategoryDto> result = categoryService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingDetails_shouldOptionalDto() {
        // Given
        Category category = new Category(2, "Name");

        when(categoryDao.findById(anyInt())).thenReturn(Optional.of(category));

        // When
        Optional<CategoryDto> result = categoryService.getDetails(2);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Name", result.get().getName());
            assertEquals(CategoryDto.class, result.get().getClass());
        });
    }

    @Test
    void whenGettingAllInstances_shouldReturnAsAListOfDto() {
        // Given
        List<Category> categories = new ArrayList<>();
        categories.add(new Category(2, "Name"));

        when(categoryDao.findAll()).thenReturn(categories);

        // When
        List<CategoryDto> result = categoryService.getAll();

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertEquals("Name", result.get(0).getName());
            assertEquals(CategoryDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenAddingInvalidCategory_shouldThrowException() {
        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.add(new CategoryDto(" ", "book")));
    }

    @Test
    void whenUpdatingInvalidCategory_shouldThrowException() {
        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.add(new CategoryDto("ODhbr1M9x88SeX9XguynOkU", " ", "book")));
    }

    @Test
    void whenTryingTORemoveAnUnknownCategory_shouldthrowException() {
        // When
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.remove(1));
    }
}