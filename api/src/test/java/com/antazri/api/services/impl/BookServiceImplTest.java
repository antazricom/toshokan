package com.antazri.api.services.impl;

import com.antazri.api.dto.BookDto;
import com.antazri.api.services.BookService;
import com.antazri.data.AuthorDao;
import com.antazri.data.BookDao;
import com.antazri.data.CategoryDao;
import com.antazri.data.impl.AuthorDaoImpl;
import com.antazri.data.impl.BookDaoImpl;
import com.antazri.data.impl.CategoryDaoImpl;
import com.antazri.model.Author;
import com.antazri.model.Book;
import com.antazri.model.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-api-test.xml"})
class BookServiceImplTest {

    private final BookDao bookDao = mock(BookDaoImpl.class);
    private final AuthorDao authorDao = mock(AuthorDaoImpl.class);
    private final CategoryDao categoryDao = mock(CategoryDaoImpl.class);
    private final BookService bookService = new BookServiceImpl(bookDao, categoryDao, authorDao);

    @Test
    void whenIdIsNotFound_shouldReturnEmptyOptional() {
        // Given
        when(bookDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<BookDto> result = bookService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingById_shouldReturnDtoWithMaskedId() {
        // Given
        Book book = new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test"));

        when(bookDao.findById(anyInt())).thenReturn(Optional.of(book));

        // When
        Optional<BookDto> result = bookService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Test", result.get().getTitle());
            assertEquals("1234567891123", result.get().getIsbn());
            assertEquals(BookDto.class, result.get().getClass());
        });
    }

    @Test
    void whenSearchingWithWrongIsbn_shouldThrowException() {
        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.getByIsbn("123"));
    }

    @Test
    void whenIsbnIsNotFound_shouldReturnEmptyOptional() {
        // Given
        when(bookDao.findByIsbn(anyString())).thenReturn(Optional.empty());

        // When
        Optional<BookDto> result = bookService.getByIsbn("1234567891123");

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByIsbn_shouldReturnInstanceAsDto() {
        // Given
        Book book = new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test"));

        when(bookDao.findByIsbn(anyString())).thenReturn(Optional.of(book));

        // When
        Optional<BookDto> result = bookService.getByIsbn("1234567891123");

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Test", result.get().getTitle());
            assertEquals("1234567891123", result.get().getIsbn());
            assertEquals(BookDto.class, result.get().getClass());
        });
    }

    @Test
    void whenSearchingByTitle_shouldReturnDtoList() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test")));

        when(bookDao.findByTitle(anyString())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByTitle("test");

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingByPublicationYear_shouldReturnDtoList() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test")));

        when(bookDao.findByPublicationYear(anyInt())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByPublicationYear(2000);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingWithInvalidAuthor_shouldReturnEmptyList() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<BookDto> result = bookService.getByAuthor(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingByAuthor_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test")));

        when(authorDao.findById(anyInt())).thenReturn(Optional.of(new Author(1, "Lorem", "Ipsum")));
        when(bookDao.findByAuthor(any())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByAuthor(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingWithInvalidCategory_shouldReturnEmptyList() {
        // Given
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<BookDto> result = bookService.getByCategory(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingByCategory_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test")));

        when(categoryDao.findById(anyInt())).thenReturn(Optional.of(new Category(1, "Lorem")));
        when(bookDao.findByCategory(any())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByCategory(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenGettingAllinstances_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                new Author(1, "Lorem", "Ipsum"),
                2000,
                "1234567891123",
                new Category(1, "Test")));

        when(bookDao.findAll()).thenReturn(books);

        // When
        List<BookDto> result = bookService.getAll();

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenAddingWithInvalidIsbn_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "Test",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2000,
                "145",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenAddingWithInvalidTitle_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2000,
                "1234567891123",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenAddingWithInvalidYear_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "Test",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2500,
                "1234567891123",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenUpdatingWithInvalidIsbn_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "ODhbr1M9x88SeX9XguynOkU",
                "Test",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2000,
                "145",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenUpdatingWithInvalidTitle_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "ODhbr1M9x88SeX9XguynOkU",
                "",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2000,
                "1234567891123",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenUpdatingWithInvalidYear_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "ODhbr1M9x88SeX9XguynOkU",
                "Test",
                "1",
                "Lorem",
                "",
                "Ipsum",
                2500,
                "1234567891123",
                "1",
                "Test");

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.add(book));
    }

    @Test
    void whenFindingNoBookWhileRemoving_shouldThrowException() {
        // When
        when(bookDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> bookService.remove(1));
    }

    @Test
    void whenRemovingBook_shouldRemoveQuietly() {
        // When
        when(bookDao.findById(anyInt()))
                .thenReturn(Optional.of(new Book(
                        5,
                        "Test",
                        new Author(5, "Lorem", "Ipsum"),
                        2000,
                        "1234567891123",
                        new Category(5, "Test")))
                );
        doNothing().when(bookDao).delete(any(Book.class));

        // Then
        assertDoesNotThrow(() -> bookService.remove(1));
    }
}