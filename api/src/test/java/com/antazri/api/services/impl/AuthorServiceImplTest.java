package com.antazri.api.services.impl;

import com.antazri.api.dto.AuthorDto;
import com.antazri.data.AuthorDao;
import com.antazri.data.impl.AuthorDaoImpl;
import com.antazri.model.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-api-test.xml"})
class AuthorServiceImplTest {

    private final AuthorDao authorDao = mock(AuthorDaoImpl.class);
    private final AuthorServiceImpl authorService = new AuthorServiceImpl(authorDao);

    @Test
    void whenValidatingAddRequest_shouldThrowException() {
        // Given
        AuthorDto author = new AuthorDto();
        author.setFirstname("Lorem");

        // Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> authorService.add(author));
    }

    @Test
    void whenValidatingUpdateRequest_shouldThrowException() {
        // When
        AuthorDto author = new AuthorDto("cHClKKtJr3Qk2I7q3-nSAFY", "Lorem", null, "");

        // Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> authorService.update(author));
    }

    @Test
    void whenValidatingUpdateRequestWithUnknownAuthor_shouldThrowException() {
        // Given
        AuthorDto author = new AuthorDto("cHClKK", "Lorem", null, "Ipsum");

        // When
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> authorService.update(author));
    }

    @Test
    void whenGettingDetails_shouldReturnAsDto() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.of(new Author(1, "Lorem", null, "Ipsum")));

        // When
        Optional<AuthorDto> result = authorService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertEquals(AuthorDto.class, result.get().getClass());
            assertTrue(result.get().getId().length() > 1);
            assertEquals("Lorem", result.get().getFirstname());
        });
    }

    @Test
    void whenNotFoundingAuthor_shouldReturnEmptyOptional() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<AuthorDto> result = authorService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnEmptyLustIfNothingFound() {
        // Given
        when(authorDao.findByName(anyString())).thenReturn(Collections.emptyList());

        // When
        List<AuthorDto> result = authorService.searchByName("test");

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnListOfDTOs() {
        // Given
        List<Author> authors = new ArrayList<>();
        authors.add(new Author(1, "Lorem", null, "Ipsum"));
        when(authorDao.findByName(anyString())).thenReturn(authors);

        // When
        List<AuthorDto> dtos = authorService.searchByName("test");

        // Then
        assertAll(() -> {
            assertEquals(1, dtos.size());
            assertEquals(AuthorDto.class, dtos.get(0).getClass());
        });
    }

    @Test
    void whenRemovingWithUnknownId_shouldThrowException() {
        // When
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> authorService.remove(1));
    }
}