package com.antazri.api.services.impl;

import com.antazri.api.dto.ArtistDto;
import com.antazri.data.ArtistDao;
import com.antazri.data.impl.ArtistDaoImpl;
import com.antazri.model.Artist;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-api-test.xml"})
class ArtistServiceImplTest {

    private final ArtistDao dao = mock(ArtistDaoImpl.class);
    private final ArtistServiceImpl artistService = new ArtistServiceImpl(dao);

    @Test
    void whenAddingWithInvalidRequest_shouldThrowException() {
        // Given
        ArtistDto artist = new ArtistDto();
        artist.setName("    ");

        // Then
        assertThrows(IllegalArgumentException.class, () -> artistService.add(artist));
    }

    @Test
    void whenUpdatingWithInvalidRequest_shouldThrowException() {
        // Given
        ArtistDto artist = new ArtistDto();
        artist.setName("    ");

        // Then
        assertThrows(IllegalArgumentException.class, () -> artistService.update(artist));
    }

    @Test
    void whenGettingDetails_shouldReturnAsDto() {
        // Given
        when(dao.findById(anyInt())).thenReturn(Optional.of(new Artist(1, "Test")));

        // When
        Optional<ArtistDto> details = artistService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(details.isPresent());
            assertEquals(ArtistDto.class, details.get().getClass());
            assertTrue(details.get().getId().length() > 1);
            assertEquals("Test", details.get().getName());
        });
    }

    @Test
    void whenNotFoundingArtist_shouldReturnEmptyOptional() {
        // Given
        when(dao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<ArtistDto> details = artistService.getDetails(1);

        // Then
        assertTrue(details.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnEmptyLustIfNothingFound() {
        // Given
        when(dao.findByName(anyString())).thenReturn(Collections.emptyList());

        // When
        List<ArtistDto> artists = artistService.searchByName("test");

        // Then
        assertTrue(artists.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnListOfDTOs() {
        // Given
        List<Artist> artists = new ArrayList<>();
        artists.add(new Artist(1, "Test 1"));
        when(dao.findByName(anyString())).thenReturn(artists);

        // When
        List<ArtistDto> dtos = artistService.searchByName("test");

        // Then
        assertAll(() -> {
            assertEquals(1, dtos.size());
            assertEquals(ArtistDto.class, dtos.get(0).getClass());
        });
    }

    @Test
    void whenRemovingWithUnknownId_shouldThrowException() {
        // When
        when(dao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> artistService.remove(1));
    }
}