package com.antazri.api.services.impl;

import com.antazri.api.dto.VinylDto;
import com.antazri.api.services.VinylService;
import com.antazri.data.ArtistDao;
import com.antazri.data.CategoryDao;
import com.antazri.data.VinylDao;
import com.antazri.data.impl.ArtistDaoImpl;
import com.antazri.data.impl.CategoryDaoImpl;
import com.antazri.data.impl.VinylDaoImpl;
import com.antazri.model.Artist;
import com.antazri.model.Category;
import com.antazri.model.Vinyl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext-api-test.xml"})
class VinylServiceImplTest {

    private final VinylDao vinylDao = mock(VinylDaoImpl.class);
    private final ArtistDao artistDao = mock(ArtistDaoImpl.class);
    private final CategoryDao categoryDao = mock(CategoryDaoImpl.class);
    private final VinylService vinylService = new VinylServiceImpl(vinylDao, artistDao, categoryDao);

    @Test
    void whenGettingDetailsWithUnknownId_shouldReturnEmptyOptional() {
        // Given
        when(vinylDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<VinylDto> result = vinylService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingDetails_shouldDtoInOptional() {
        // Given
        Vinyl vinyl = new Vinyl(
                1,
                "Title",
                new Artist(1, "Artist"),
                2000,
                new Category(1, "Category")
        );

        when(vinylDao.findById(anyInt())).thenReturn(Optional.of(vinyl));

        // When
        Optional<VinylDto> result = vinylService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Title", result.get().getTitle());
        });
    }

    @Test
    void whenSearchingByTitleWithEmptyString_shouldReturnEmptyList() {
        // Given
        when(vinylDao.findByTitle("")).thenReturn(Collections.emptyList());

        // When
        List<VinylDto> result = vinylService.getByTitle(" ");

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByTitle_shouldReturnListOfDto() {
        // Given
        List<Vinyl> vinyls = new ArrayList<>();
        vinyls.add(
                new Vinyl(
                        1,
                        "Title",
                        new Artist(1, "Artist"),
                        2000,
                        new Category(1, "Category")
                )
        );
        when(vinylDao.findByTitle(anyString())).thenReturn(vinyls);

        // When
        List<VinylDto> result = vinylService.getByTitle("test");

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Title", result.get(0).getTitle());
        });
    }

    @Test
    void whenSearchingByArtistNotFound_shouldReturnEmptyList() {
        // Given
        when(artistDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<VinylDto> result = vinylService.getByArtist(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByArtist_shouldReturnListOfDto() {
        // Given
        List<Vinyl> vinyls = new ArrayList<>();
        vinyls.add(
                new Vinyl(
                        1,
                        "Title",
                        new Artist(1, "Artist"),
                        2000,
                        new Category(1, "Category")
                )
        );

        when(artistDao.findById(anyInt())).thenReturn(Optional.of(new Artist(1, "Test")));
        when(vinylDao.findByArtist(any(Artist.class))).thenReturn(vinyls);

        // When
        List<VinylDto> result = vinylService.getByArtist(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Title", result.get(0).getTitle());
        });
    }

    @Test
    void whenSearchingByCategoryNotFound_shouldReturnEmptyList() {
        // Given
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<VinylDto> result = vinylService.getByCategory(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByCategory_shouldReturnListOfDto() {
        // Given
        List<Vinyl> vinyls = new ArrayList<>();
        vinyls.add(
                new Vinyl(
                        1,
                        "Title",
                        new Artist(1, "Artist"),
                        2000,
                        new Category(1, "Category")
                )
        );

        when(categoryDao.findById(anyInt())).thenReturn(Optional.of(new Category(1, "Category", "book")));
        when(vinylDao.findByCategory(any(Category.class))).thenReturn(vinyls);

        // When
        List<VinylDto> result = vinylService.getByCategory(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Title", result.get(0).getTitle());
        });
    }

    @Test
    void whenFindingAllInstances_shouldReturnListOfDto() {
        // Given
        List<Vinyl> vinyls = new ArrayList<>();
        vinyls.add(
                new Vinyl(
                        1,
                        "Title",
                        new Artist(1, "Artist"),
                        2000,
                        new Category(1, "Category")
                )
        );
        when(vinylDao.findAll()).thenReturn(vinyls);

        // When
        List<VinylDto> result = vinylService.getAll();

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Title", result.get(0).getTitle());
        });
    }

    @Test
    void whenSearchingByPublicationYear_shouldReturnListOfDto() {
        // Given
        List<Vinyl> vinyls = new ArrayList<>();
        vinyls.add(
                new Vinyl(
                        1,
                        "Title",
                        new Artist(1, "Artist"),
                        2000,
                        new Category(1, "Category")
                )
        );
        when(vinylDao.findByPublicationYear(anyInt())).thenReturn(vinyls);

        // When
        List<VinylDto> result = vinylService.getByPublicationYear(2000);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Title", result.get(0).getTitle());
        });
    }

    @Test
    void whenAddingNewVinylWithInvalidField_shouldThrowException() {
        // When
        VinylDto vinyl = new VinylDto(
                null,
                "ODhbr1M9x88SeX9XguynOkU",
                "Artist",
                2000,
                "ODhbr1M9x88SeX9XguynOkU",
                "Category"
        );

        // Then
        assertThrows(IllegalArgumentException.class, () -> vinylService.add(vinyl));
    }

    @Test
    void whenUpdatingVinylWithInvalidField_shouldThrowException() {
        // When
        VinylDto vinyl = new VinylDto(
                "ODhbr1M9x88SeX9XguynOkU",
                null,
                "ODhbr1M9x88SeX9XguynOkU",
                "Artist",
                2000,
                "ODhbr1M9x88SeX9XguynOkU",
                "Category"
        );

        // Then
        assertThrows(IllegalArgumentException.class, () -> vinylService.update(vinyl));
    }

    @Test
    void whenRemovingAnUnknownVinyl_shouldThrowException() {
        // When
        when(vinylDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> vinylService.remove(1));
    }
}