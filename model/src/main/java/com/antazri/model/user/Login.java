package com.antazri.model.user;

import com.antazri.model.security.Role;
import com.antazri.model.security.RoleAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Entity
@Table(name = "login")
@NamedQueries({
        @NamedQuery(name = "Login.FindByEmail", query = "SELECT l FROM Login l WHERE l.email = :email")
})
public class Login {

    @Id
    @SequenceGenerator(name = "login_id_seq", sequenceName = "login_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "login_id_seq")
    private Integer id;

    @Pattern(regexp = "^(.+)@(.+)$")
    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Convert(converter = RoleAttributeConverter.class)
    @Column(name = "role", nullable = false)
    private Role role;

    public Login() {
    }

    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Login(Integer id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFormattedRoleName() {
        return String.join("", "ROLE_", this.role.getName()).toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Login login = (Login) o;
        return Objects.equals(id, login.id)
                && Objects.equals(email, login.email)
                && Objects.equals(password, login.password)
                && role == login.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, role);
    }

    @Override
    public String toString() {
        return "Login{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
