package com.antazri.model;

import com.antazri.model.entity.TemporalEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "artist")
@NamedQueries({
        @NamedQuery(name = "Artist.FindAll", query = "SELECT a FROM Artist a"),
        @NamedQuery(name = "Artist.FindByName", query = "SELECT a FROM Artist a WHERE lower(a.name) LIKE lower(:name)")
})
public class Artist extends TemporalEntity {

    @Id
    @SequenceGenerator(name = "artist_id_seq", sequenceName = "artist_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "artist_id_seq")
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(name = "name", unique = true)
    private String name;

    public Artist() {
    }

    public Artist(String name) {
        this.name = name;
    }

    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return id == artist.id
                && Objects.equals(name, artist.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
