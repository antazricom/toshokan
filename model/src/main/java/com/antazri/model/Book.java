package com.antazri.model;

import com.antazri.model.entity.TemporalEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "book")
@NamedQueries({
        @NamedQuery(name = "Book.FindAll", query = "SELECT b FROM Book b"),
        @NamedQuery(name = "Book.FindByAuthor", query = "SELECT b FROM Book b WHERE b.author.id = :id"),
        @NamedQuery(name = "Book.FindByCategory", query = "SELECT b FROM Book b WHERE b.category.id = :id"),
        @NamedQuery(name = "Book.FindByIsbn", query = "SELECT b FROM Book b WHERE b.isbn = :isbn"),
        @NamedQuery(name = "Book.FindByPublicationYear", query = "SELECT b FROM Book b WHERE b.publicationYear = :year"),
        @NamedQuery(name = "Book.FindByTitle", query = "SELECT b FROM Book b WHERE lower(b.title) LIKE lower(:title)"),
        @NamedQuery(name = "Book.FindByDateRange", query = "SELECT b FROM Book b WHERE b.publicationYear BETWEEN :start AND :end")
})
public class Book extends TemporalEntity {

    @Id
    @SequenceGenerator(name = "book_id_seq", sequenceName = "book_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_id_seq")
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private Author author;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @Column(name = "isbn", unique = true)
    private String isbn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    public Book() {
    }

    public Book(String title, Author author, Integer publicationYear, String isbn, Category category) {
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.category = category;
    }

    public Book(Integer id, String title, Author author, Integer publicationYear, String isbn, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id
                && publicationYear == book.publicationYear
                && Objects.equals(title, book.title)
                && Objects.equals(author, book.author)
                && Objects.equals(isbn, book.isbn)
                && Objects.equals(category, book.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, author, publicationYear, isbn, category);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author=" + author +
                ", publicationYear=" + publicationYear +
                ", isbn='" + isbn + '\'' +
                ", category=" + category +
                '}';
    }
}
