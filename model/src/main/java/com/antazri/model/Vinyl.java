package com.antazri.model;

import com.antazri.model.entity.TemporalEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "vinyl")
@NamedQueries({
        @NamedQuery(name = "Vinyl.FindAll", query = "SELECT v FROM Vinyl v"),
        @NamedQuery(name = "Vinyl.FindByArtist", query = "SELECT v FROM Vinyl v WHERE v.artist.id = :id"),
        @NamedQuery(name = "Vinyl.FindByCategory", query = "SELECT v FROM Vinyl v WHERE v.category.id = :id"),
        @NamedQuery(name = "Vinyl.FindByPublicationYear", query = "SELECT v FROM Vinyl v WHERE v.publicationYear = :year"),
        @NamedQuery(name = "Vinyl.FindByTitle", query = "SELECT v FROM Vinyl v WHERE lower(v.title) LIKE lower(:title) "),
        @NamedQuery(name = "Vinyl.FindByDateRange", query = "SELECT v FROM Vinyl v WHERE v.publicationYear BETWEEN :start AND :end")
})
public class Vinyl extends TemporalEntity {

    @Id
    @SequenceGenerator(name = "vinyl_id_seq", sequenceName = "vinyl_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vinyl_id_seq")
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    private Artist artist;

    @NotBlank
    @Column(name = "publication_year")
    private Integer publicationYear;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    public Vinyl() {
    }

    public Vinyl(String title, Artist artist, Integer publicationYear, Category category) {
        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public Vinyl(Integer id, String title, Artist artist, Integer publicationYear, Category category) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vinyl vinyl = (Vinyl) o;
        return id == vinyl.id
                && publicationYear == vinyl.publicationYear
                && Objects.equals(title, vinyl.title)
                && Objects.equals(artist, vinyl.artist)
                && Objects.equals(category, vinyl.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, artist, publicationYear, category);
    }

    @Override
    public String toString() {
        return "Vinyl{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", artist=" + artist +
                ", publicationYear=" + publicationYear +
                ", category=" + category +
                '}';
    }
}
