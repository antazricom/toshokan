package com.antazri.model.security;

public enum Role {

    ADMIN("ADMIN");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    public String getName() {
        return "ROLE_" + name;
    }
}
