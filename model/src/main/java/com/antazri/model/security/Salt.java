package com.antazri.model.security;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

@Entity
@Table(name = "salt")
@NamedQueries({
        @NamedQuery(name = "Salt.FindByLogin", query = "SELECT s FROM Salt s WHERE s.loginId = :id")
})
public class Salt {

    @Id
    @JoinColumn(name = "login_id", referencedColumnName = "id", table = "login", nullable = false)
    private Integer loginId;

    @Min(8)
    @Max(16)
    @Column(name = "value", nullable = false)
    private String value;

    public Salt() {
    }

    public Salt(Integer loginId, String value) {
        this.loginId = loginId;
        this.value = value;
    }

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Salt salt = (Salt) o;
        return Objects.equals(loginId, salt.loginId) && Objects.equals(value, salt.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loginId, value);
    }

    @Override
    public String toString() {
        return "Salt{" +
                "loginId=" + loginId +
                ", value='" + value + '\'' +
                '}';
    }
}
